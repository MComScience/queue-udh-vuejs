import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

const HelloWord = require('../components/user/HelloWord');
const UserIndex = require('../components/user/Index');
const UserForm = require('../components/user/Form');
const NotFound = Vue.component('button-counter', {
    created: function () {
        document.title = '404 Not found';
    },
    template: `<div class="error-container">
                    <i class="pe-7s-way text-danger big-icon"></i>
                    <h1>404</h1>
                    <strong>Page Not Found</strong>
               </div>`
});

let routes = [
    {path: '/', component: HelloWord, name: 'แดชบอร์ด'},
    {path: '/user/admin/index', component: UserIndex, name: 'ผู้ใช้งาน'},
    {path: '/user/admin/create', component: UserForm, name: 'เพิ่มผู้ใช้งาน'},
    {path: '*', component: NotFound, name: '404 Not found'}
];

let router = new VueRouter({
    mode: 'history',
    routes
});

export default router;
