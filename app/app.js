import Vue from 'vue'
import router from './router'

require('./bootstrap')

new Vue({
    el: '#app',
    router,
    methods: {
        isActiveMenu(path) {
            return window.location.pathname == path;
        }
    }
})