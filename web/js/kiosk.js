//Socket Events
$(function () {
    socket.on('register', (res) => { //อกกบัตรคิว
        var table = $('#tb-que-list').DataTable();
        table.ajax.reload();
    });
});
//$('body').addClass('hide-sidebar');

var isEmpty = function (v) {
    return v === undefined || v === null || v.length === 0 || v === '';
};

var app = new Vue({
    el: '#app',
    data: {
        search_by: '0',
        que_hn: null,
        service_id: null,
        service_group_id: null,
        services: [],
        loading: true,
        formIsValid: true,
    },
    methods: {
        onChangeCheckbox: function (event) {
            this.search_by = event.target.value;
        },
        onKeyupInputGroup: function (event) {
            this.formIsValid = isEmpty(event.target.value);
            this.que_hn = event.target.value;
            this.service_id = $(event.target).data('service_id');
            this.service_group_id = $(event.target).data('service_group_id');
            this.$refs.input.forEach(element => {
                if ($(element).attr('id') !== $(event.target).attr('id')) {
                    $(element).val(null);
                }
            });
        },
        onSubmit: function () {
            var self = this;
            $.ajax({
                url: '/app/kiosk/search-data',
                type: 'POST',
                data: {
                    que_hn: this.que_hn,
                    service_id: this.service_id,
                    service_group_id: this.service_group_id,
                    search_by: this.search_by,
                },
                dataType: 'json',
                success: function (response) {
                    if (response.success) {
                        swal({
                            title: 'พิมพ์บัตรคิว?',
                            text: response.personal.name,
                            html: '<p>' + response.modelService.service_name + '</p>' + '<p><i class="fa fa-user"></i> ' + response.personal.name + '</p>',
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'พิมพ์',
                            cancelButtonText: 'ยกเลิก',
                            allowOutsideClick: false,
                            showLoaderOnConfirm: true,
                            preConfirm: function () {
                                return new Promise(function (resolve, reject) {
                                    $.ajax({
                                        method: "POST",
                                        url: '/app/kiosk/save-search',
                                        data: response,
                                        dataType: "json",
                                        success: function (response) {
                                            var table = $('#tb-queue-list').DataTable();
                                            table.ajax.reload();
                                            $('#ajaxCrudModal').modal('hide');
                                            socket.emit('register', response);
                                            //window.open(response.url, "myPrint", "width=800, height=600");
                                            //$('#form-search').trigger('reset');
                                            self.onClear();
                                            resolve();
                                        },
                                        error: function (jqXHR, textStatus, errorThrown) {
                                            self.onClear();
                                            //$('#form-search').trigger('reset');
                                            swal({
                                                type: 'error',
                                                title: textStatus,
                                                text: errorThrown,
                                            });
                                        }
                                    });
                                });
                            },
                        }).then((result) => {
                            if (result.value) { //Confirm
                                swal.close();
                            } else {
                                //$('#form-search').trigger('reset');
                            }
                        });
                    } else {
                        self.onClear();
                        swal({
                            type: 'error',
                            title: response.message || 'เกิดข้อผิดพลาด',
                            text: '',
                        });
                        //$('#form-search').trigger('reset');
                    }
                },
                error: function (jqXHR, errMsg) {
                    self.onClear();
                    //$('#form-search').trigger('reset');
                    swal({
                        type: 'error',
                        title: 'Oops...',
                        text: errMsg,
                    });
                }
            });
        },
        onClear: function () {
            this.formIsValid = true;
            this.que_hn = null;
            this.service_id = null;
            this.service_group_id = null;
            this.$refs.input.forEach(element => {
                $(element).val(null);
            });
        },
        fetchServiceGroup: function () {
            axios
                .get('/app/kiosk/data-services?id=' + $('#tbservicegroup-_id').val())
                .then(response => {
                    this.services = response.data;
                    setTimeout(() => {
                        this.loading = false;
                    }, 1000);
                })
                .catch(error => {
                    this.loading = false;
                    swal({
                        type: 'error',
                        title: 'Oops...',
                        text: error.response.statusText,
                    });
                });
        }
    },
    mounted() {
        this.fetchServiceGroup()
    }
});

var $form = $('#form-search');
$form.on('beforeSubmit', function () {
    return false; // prevent default submit
});