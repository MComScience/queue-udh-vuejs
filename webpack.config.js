const path = require('path');
const webpack = require('webpack');

const PATHS = {
    source: path.join(__dirname, 'app'),
    build: path.join(__dirname, 'web')
};

module.exports = {
    output: {
        path: PATHS.build,
        filename: 'app.js'
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        })
    ],
};
