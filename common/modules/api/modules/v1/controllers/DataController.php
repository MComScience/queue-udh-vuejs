<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 25/2/2562
 * Time: 11:23
 */

namespace common\modules\api\modules\v1\controllers;

use common\modules\app\models\TbCounterService;
use common\modules\app\models\TbCounterServiceType;
use common\modules\app\models\TbDisplay;
use common\modules\app\models\TbQueue;
use common\modules\app\models\TbQueueStatus;
use common\modules\app\models\TbService;
use common\modules\app\models\TbServiceGroup;
use common\modules\app\models\TbServiceProfile;
use common\modules\app\models\TbSound;
use common\modules\app\models\TbSoundStation;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\rest\ActiveController;
use yii\web\MethodNotAllowedHttpException;
use yii\web\Response;
use homer\widgets\tablecolumn\ActionTable;
use homer\widgets\tablecolumn\ColumnData;
use yii\mongodb\Query;
use common\components\AccessRule;
use kartik\icons\Icon;
use common\modules\app\models\TbTicket;

class DataController extends ActiveController
{
    public $modelClass = '';

    public function actions()
    {
        $actions = parent::actions();

        // disable the "delete" and "create" actions
        unset($actions['delete'], $actions['create'], $actions['update']);

        return $actions;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],

        ];
        $behaviors['verbs'] = [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'index' => ['get'],
                'view' => ['get'],
                'create' => ['post'],
                'update' => ['put'],
                'delete' => ['delete'],
                'info' => ['get'],
                'data-service-group' => ['get'],
                'data-ticket' => ['get'],
                'data-counter-service' => ['get'],
                'data-sound-station' => ['get'],
                'data-service-profile' => ['get'],
                'data-display' => ['get'],
                'data-queue' => ['get'],
            ],
        ];
        // remove authentication filter
        $auth = $behaviors['authenticator'];
        unset($behaviors['authenticator']);
        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
            ],
        ];
        // re-add authentication filter
        $behaviors['authenticator'] = $auth;
        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
        $behaviors['authenticator']['except'] = [
            'options',
            'data-service-group',
            'data-ticket',
            'data-counter-service',
            'data-sound-station',
            'data-service-profile',
            'data-display',
            'data-queue'
        ];
        // setup access
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'ruleConfig' => [
                'class' => AccessRule::className()
            ],
            'only' => ['index', 'view', 'create', 'update', 'delete'], //only be applied to
            'rules' => [
                [
                    'allow' => true,
                    'actions' => ['index', 'view', 'create', 'update', 'delete', 'info',],
                    'roles' => ['@'],
                ],
            ],
        ];
        return $behaviors;
    }

    public function actionDataServiceGroup()
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            $serviceGroups = TbServiceGroup::find()->all();
            $result = [];
            foreach ($serviceGroups as $serviceGroup) {
                $services = TbService::find()->where(['service_group_id' => (string)$serviceGroup['_id']])->all();
                foreach ($services as $index => $service) {
                    $ticket = TbTicket::findOne(['_id' => $service['print_template_id']]);
                    $result[] = [
                        'service_group_id' => (string)$serviceGroup['_id'],
                        'service_group_name' => $serviceGroup['service_group_name'],
                        'service_group_status' => $serviceGroup['service_group_status'],
                        'service_name' => $service['service_name'],
                        'print_template_id' => $service['print_template_id'],
                        'print_copy_qty' => $service['print_copy_qty'],
                        'service_prefix' => $service['service_prefix'],
                        'service_numdigit' => $service['service_numdigit'],
                        'service_status' => $service['service_status'],
                        'avg_time' => $service['avg_time'],
                        'avg_time_more' => $service['avg_time_more'],
                        'service_id' => (string)$service['_id'],
                        'hos_name_th' => $ticket['hos_name_th']
                    ];
                }
            }
            $dataProvider = new ArrayDataProvider([
                'allModels' => $result,
                'pagination' => [
                    'pageSize' => false,
                ],
                'key' => 'service_group_id',
            ]);
            $columns = Yii::createObject([
                'class' => ColumnData::className(),
                'dataProvider' => $dataProvider,
                'formatter' => Yii::$app->formatter,
                'columns' => [
                    [
                        'attribute' => 'service_group_id',
                    ],
                    [
                        'attribute' => 'service_group_name',
                    ],
                    [
                        'attribute' => 'service_group_status',
                    ],
                    [
                        'attribute' => 'service_id',
                    ],
                    [
                        'attribute' => 'service_name',
                    ],
                    [
                        'attribute' => 'service_group_id',
                    ],
                    [
                        'attribute' => 'print_template_id',
                    ],
                    [
                        'attribute' => 'print_copy_qty',
                    ],
                    [
                        'attribute' => 'service_prefix',
                    ],
                    [
                        'attribute' => 'service_numdigit',
                    ],
                    [
                        'attribute' => 'avg_time',
                    ],
                    [
                        'attribute' => 'avg_time_more',
                    ],
                    [
                        'attribute' => 'service_status',
                        'value' => function ($model, $key, $index) {
                            return $this->getBadgeStatus($model['service_status']);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'hos_name_th',
                    ],
                    [
                        'class' => ActionTable::className(),
                        'template' => '<div class="btn-group" role="group">{update} {delete}</div>',
                        'updateOptions' => [
                            'class' => 'btn btn-default',
                            'role' => 'modal-remote',
                            'title' => 'แก้ไข',
                        ],
                        'deleteOptions' => [
                            'class' => 'btn btn-default text-danger activity-delete',
                            'title' => 'ลบ',
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action == 'update') {
                                return Url::to(['/app/settings/update-service-group', 'id' => $key]);
                            }
                            if ($action == 'delete') {
                                return Url::to(['/app/settings/delete-service-group', 'id' => $key, 'service_id' => $model['service_id']]);
                            }
                        },
                    ],
                ],
            ]);
            return ['data' => $columns->renderDataColumns()];
        } else {
            throw  new MethodNotAllowedHttpException('method not allowed.');
        }
    }

    public function actionDataTicket()
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            $result = [];
            $modelTickets = TbTicket::find()->all();
            foreach ($modelTickets as $ticket) {
                $result[] = [
                    'ticket_ids' => (string)$ticket['_id'],
                    'hos_name_th' => $ticket['hos_name_th'],
                    'hos_name_en' => $ticket['hos_name_en'],
                    'template' => $ticket['template'],
                    'ticket_status' => $ticket['ticket_status'],
                ];
            }
            $dataProvider = new ArrayDataProvider([
                'allModels' => $result,
                'pagination' => [
                    'pageSize' => false,
                ],
                'key' => 'ticket_ids',
            ]);
            $columns = Yii::createObject([
                'class' => ColumnData::className(),
                'dataProvider' => $dataProvider,
                'formatter' => Yii::$app->formatter,
                'columns' => [
                    [
                        'attribute' => 'ticket_ids',
                    ],
                    [
                        'attribute' => 'hos_name_th',
                    ],
                    [
                        'attribute' => 'hos_name_en',
                    ],
                    [
                        'attribute' => 'ticket_status',
                        'value' => function ($model, $key, $index) {
                            return $this::getBadgeStatus($model['ticket_status']);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => ActionTable::className(),
                        'template' => '<div class="btn-group" role="group">{copy} {update} {delete}</div>',
                        'updateOptions' => [
                            'title' => 'แก้ไข',
                            'class' => 'btn btn-default text-info',
                        ],
                        'deleteOptions' => [
                            'class' => 'btn btn-default text-danger activity-delete',
                            'title' => 'ลบ',
                        ],
                        'buttons' => [
                            'copy' => function ($url, $model, $key) {
                                return Html::a(Icon::show('copy'), ['/app/settings/copy-ticket', 'id' => $key], ['class' => 'btn btn-default text-primary activity-copy', 'title' => 'Copy']);
                            }
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action == 'update') {
                                return Url::to(['/app/settings/update-ticket', 'id' => $key]);
                            }
                            if ($action == 'delete') {
                                return Url::to(['/app/settings/delete-ticket', 'id' => $key]);
                            }
                        },
                    ],
                ],
            ]);
            return ['data' => $columns->renderDataColumns()];
        } else {
            throw  new MethodNotAllowedHttpException('method not allowed.');
        }
    }

    public function actionDataCounterService()
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            $result = [];
            $counterServiceTypes = TbCounterServiceType::find()->all();
            foreach ($counterServiceTypes as $counterServiceType) {
                $counterServices = TbCounterService::find()->where(['counter_service_type_id' => (string)$counterServiceType['_id']])->all();
                foreach ($counterServices as $counterService) {
                    $soundNumber = TbSound::findOne($counterService['sound_number_id']);
                    $soundService = TbSound::findOne($counterService['sound_service_id']);
                    $serviceGroup = TbServiceGroup::findOne($counterService['service_group_id']);
                    $result[] = [
                        'counter_service_type_id' => (string)$counterServiceType['_id'],
                        'counter_service_type_name' => $counterServiceType['counter_service_type_name'],
                        'counter_service_id' => (string)$counterService['_id'],
                        'counter_service_name' => $counterService['counter_service_name'],
                        'counter_service_call_number' => $counterService['counter_service_call_number'],
                        'service_group_name' => $serviceGroup['service_group_name'],
                        'sound_name1' => $soundService['sound_label'],
                        'sound_name2' => $soundNumber['sound_label'],
                        'counter_service_status' => $counterService['counter_service_status'],
                    ];
                }
            }
            $dataProvider = new ArrayDataProvider([
                'allModels' => $result,
                'pagination' => [
                    'pageSize' => false,
                ],
                'key' => 'counter_service_type_id',
            ]);
            $columns = Yii::createObject([
                'class' => ColumnData::className(),
                'dataProvider' => $dataProvider,
                'formatter' => Yii::$app->formatter,
                'columns' => [
                    [
                        'attribute' => 'counter_service_type_id',
                    ],
                    [
                        'attribute' => 'counter_service_type_name',
                    ],
                    [
                        'attribute' => 'counter_service_id',
                    ],
                    [
                        'attribute' => 'counter_service_name',
                    ],
                    [
                        'attribute' => 'counter_service_call_number',
                    ],
                    [
                        'attribute' => 'service_group_name',
                    ],
                    [
                        'attribute' => 'sound_name1',
                    ],
                    [
                        'attribute' => 'sound_name2',
                    ],
                    [
                        'attribute' => 'counter_service_status',
                        'value' => function ($model, $key, $index) {
                            return $this::getBadgeStatus($model['counter_service_status']);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => ActionTable::className(),
                        'template' => '<div class="btn-group" role="group">{update} {delete}</div>',
                        'updateOptions' => [
                            'role' => 'modal-remote',
                            'title' => 'แก้ไข',
                            'class' => 'btn btn-default text-info',
                        ],
                        'deleteOptions' => [
                            'class' => 'btn btn-default text-danger activity-delete',
                            'title' => 'ลบ',
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action == 'update') {
                                return Url::to(['/app/settings/update-counter-service', 'id' => $key]);
                            }
                            if ($action == 'delete') {
                                return Url::to(['/app/settings/delete-counter-service', 'counter_service_type_id' => $key, 'counter_service_id' => $model['counter_service_id']]);
                            }
                        },
                    ],
                ],
            ]);
            return ['data' => $columns->renderDataColumns()];
        } else {
            throw  new MethodNotAllowedHttpException('method not allowed.');
        }
    }

    public function actionDataSoundStation()
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            $result = [];
            $soundStations = TbSoundStation::find()->all();
            foreach ($soundStations as $soundStation) {
                $counterServices = TbCounterService::find()->where(['_id' => $soundStation['counter_service_id']])->all();
                $li = [];
                foreach ($counterServices as $key => $value) {
                    $li[] = Html::tag('li', $value['counter_service_name']);
                }
                $result[] = [
                    'sound_station_id' => (string)$soundStation['_id'],
                    'sound_station_name' => $soundStation['sound_station_name'],
                    'sound_station_status' => $soundStation['sound_station_status'],
                    'counter_service_id' => !empty($li) ? Html::tag('ul', implode("\n", $li)) : ''
                ];
            }
            $dataProvider = new ArrayDataProvider([
                'allModels' => $result,
                'pagination' => [
                    'pageSize' => false,
                ],
                'key' => 'sound_station_id',
            ]);
            $columns = Yii::createObject([
                'class' => ColumnData::className(),
                'dataProvider' => $dataProvider,
                'formatter' => Yii::$app->formatter,
                'columns' => [
                    [
                        'attribute' => 'sound_station_id',
                    ],
                    [
                        'attribute' => 'sound_station_name',
                    ],
                    [
                        'attribute' => 'counter_service_id',
                        'format' => 'raw'
                    ],
                    [
                        'attribute' => 'sound_station_status',
                        'value' => function ($model, $key, $index) {
                            return $this::getBadgeStatus($model['sound_station_status']);
                        },
                        'format' => 'raw'
                    ],
                    [
                        'class' => ActionTable::className(),
                        'template' => '<div class="btn-group" role="group">{update} {delete}</div>',
                        'updateOptions' => [
                            'role' => 'modal-remote',
                            'title' => 'แก้ไข',
                            'class' => 'btn btn-default'
                        ],
                        'deleteOptions' => [
                            'class' => 'btn btn-default text-danger activity-delete',
                            'title' => 'ลบ',
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action == 'update') {
                                return Url::to(['/app/settings/update-sound-station', 'id' => $key]);
                            }
                            if ($action == 'delete') {
                                return Url::to(['/app/settings/delete-sound-station', 'id' => $key]);
                            }
                        }
                    ],
                ],
            ]);
            return ['data' => $columns->renderDataColumns()];
        } else {
            throw  new MethodNotAllowedHttpException('method not allowed.');
        }
    }

    public function actionDataServiceProfile()
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            $result = [];
            $serviceProfiles = TbServiceProfile::find()->all();
            foreach ($serviceProfiles as $serviceProfile) {
                $services = TbService::find()->where(['_id' => $serviceProfile['service_id']])->all();
                $li = [];
                foreach ($services as $key => $value) {
                    $li[] = Html::tag('li', $value['service_name']);
                }
                $counterType = TbCounterServiceType::findOne($serviceProfile['counter_service_type_id']);
                $result[] = [
                    'service_profile_id' => (string)$serviceProfile['_id'],
                    'service_profile_name' => $serviceProfile['service_profile_name'],
                    'service_profile_status' => $serviceProfile['service_profile_status'],
                    'counter_service_type_name' => $counterType['counter_service_type_name'],
                    'service_names' => !empty($li) ? Html::tag('ul', implode("\n", $li)) : ''
                ];
            }
            $dataProvider = new ArrayDataProvider([
                'allModels' => $result,
                'pagination' => [
                    'pageSize' => false,
                ],
                'key' => 'service_profile_id',
            ]);
            $columns = Yii::createObject([
                'class' => ColumnData::className(),
                'dataProvider' => $dataProvider,
                'formatter' => Yii::$app->formatter,
                'columns' => [
                    [
                        'attribute' => 'service_profile_id',
                    ],
                    [
                        'attribute' => 'service_profile_name',
                    ],
                    [
                        'attribute' => 'counter_service_type_name',
                    ],
                    [
                        'attribute' => 'service_names',
                        'format' => 'raw'
                    ],
                    [
                        'attribute' => 'service_profile_status',
                        'value' => function ($model, $key, $index) {
                            return $this->getBadgeStatus($model['service_profile_status']);
                        },
                        'format' => 'raw'
                    ],
                    [
                        'class' => ActionTable::className(),
                        'template' => '<div class="btn-group" role="group">{update} {delete}</div>',
                        'updateOptions' => [
                            'role' => 'modal-remote',
                            'title' => 'แก้ไข',
                            'class' => 'btn btn-default text-info'
                        ],
                        'deleteOptions' => [
                            'class' => 'btn btn-default text-danger activity-delete',
                            'title' => 'ลบ',
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action == 'update') {
                                return Url::to(['/app/settings/update-service-profile', 'id' => $key]);
                            }
                            if ($action == 'delete') {
                                return Url::to(['/app/settings/delete-service-profile', 'id' => $key]);
                            }
                        }
                    ],
                ],
            ]);
            return ['data' => $columns->renderDataColumns()];
        } else {
            throw  new MethodNotAllowedHttpException('method not allowed.');
        }
    }

    public function actionDataDisplay()
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            $result = [];
            $displays = TbDisplay::find()->all();
            foreach ($displays as $display) {
                $result[] = [
                    'display_ids' => (string)$display['_id'],
                    'display_name' => $display['display_name'],
                    'display_status' => $display['display_status'],
                ];
            }
            $dataProvider = new ArrayDataProvider([
                'allModels' => $result,
                'pagination' => [
                    'pageSize' => false,
                ],
                'key' => 'display_ids',
            ]);
            $columns = Yii::createObject([
                'class' => ColumnData::className(),
                'dataProvider' => $dataProvider,
                'formatter' => Yii::$app->formatter,
                'columns' => [
                    [
                        'attribute' => 'display_ids',
                    ],
                    [
                        'attribute' => 'display_name',
                    ],
                    [
                        'attribute' => 'display_status',
                        'value' => function ($model, $key, $index) {
                            return $this->getBadgeStatus($model['display_status']);
                        },
                        'format' => 'raw'
                    ],
                    [
                        'class' => ActionTable::className(),
                        'template' => '<div class="btn-group" role="group">{duplicate} {update} {delete}</div>',
                        'updateOptions' => [
                            'title' => 'แก้ไข',
                            'class' => 'btn btn-default text-info'
                        ],
                        'deleteOptions' => [
                            'class' => 'btn btn-default text-danger activity-delete',
                            'title' => 'ลบ',
                        ],
                        'buttons' => [
                            'duplicate' => function ($url, $model, $key) {
                                return Html::a(Icon::show('copy'), ['/app/settings/copy-display', 'id' => $key], ['class' => 'btn btn-default text-primary activity-copy', 'title' => 'Duplicate']);
                            }
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action == 'update') {
                                return Url::to(['/app/settings/update-display', 'id' => $key]);
                            }
                            if ($action == 'delete') {
                                return Url::to(['/app/settings/delete-display', 'id' => $key]);
                            }
                        }
                    ],
                ],
            ]);
            return ['data' => $columns->renderDataColumns()];
        } else {
            throw  new MethodNotAllowedHttpException('method not allowed.');
        }
    }

    public function actionDataQueue()
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            $result = [];
            $queues = TbQueue::find()->all();
            foreach ($queues as $queue) {
                $service = TbService::findOne($queue['service_id']);
                $serviceGroup = TbServiceGroup::findOne($queue['service_group_id']);
                $status = TbQueueStatus::findOne($queue['queue_status_id']);
                $result[] = [
                    'que_ids' => (string)$queue['_id'],
                    'queue_no' => $queue['queue_no'],
                    'queue_hn' => $queue['queue_hn'],
                    'pt_name' => $queue['pt_name'],
                    'service_id' => $queue['service_id'],
                    'service_group_id' => $queue['service_group_id'],
                    'queue_status_id' => $queue['queue_status_id'],
                    'created_at' => $queue['created_at'],
                    'created_time' => Yii::$app->formatter->asDate($queue['created_at'], 'php:H:i:s'),
                    'service_name' => $service['service_name'],
                    'service_group_name' => $serviceGroup['service_group_name'],
                    'que_status_name' => $status['que_status_name']
                ];
            }
            $dataProvider = new ArrayDataProvider([
                'allModels' => $result,
                'pagination' => [
                    'pageSize' => false,
                ],
                'key' => 'que_ids',
            ]);
            $columns = Yii::createObject([
                'class' => ColumnData::className(),
                'dataProvider' => $dataProvider,
                'formatter' => Yii::$app->formatter,
                'columns' => [
                    [
                        'attribute' => 'que_ids',
                    ],
                    [
                        'attribute' => 'queue_no',
                    ],
                    [
                        'attribute' => 'queue_no_badge',
                        'value' => function ($model, $key, $index, $column) {
                            return \kartik\helpers\Html::badge($model['queue_no'], ['class' => 'badge badge-success', 'style' => 'font-size: 20px;font-weight: 600;width: 80px;']);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'queue_hn',
                    ],
                    [
                        'attribute' => 'pt_name',
                    ],
                    [
                        'attribute' => 'service_id',
                    ],
                    [
                        'attribute' => 'service_group_id',
                    ],
                    [
                        'attribute' => 'que_status_id',
                    ],
                    [
                        'attribute' => 'created_at',
                        'value' => function ($model, $key, $index) {
                            return Html::tag('code', Yii::$app->formatter->asDate($model['created_at'], 'php:H:i:s'));
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'created_time',
                    ],
                    [
                        'attribute' => 'service_name',
                    ],
                    [
                        'attribute' => 'service_group_name',
                    ],
                    [
                        'attribute' => 'que_status_name',
                        'value' => function ($model, $key, $index, $column) {
                            $badgeClass = 'badge';
                            switch ($model['queue_status_id']) {
                                case 1: //รอเรียก
                                    $badgeClass = 'badge badge-warning';
                                    break;
                                case 2: //กำลังเรียก
                                    $badgeClass = 'badge';
                                    break;
                                case 3: //พักคิว
                                    $badgeClass = 'badge badge-danger';
                                    break;
                                case 4: //เสร็จสิ้น
                                    $badgeClass = 'badge badge-success';
                                    break;
                                default:
                                    $badgeClass = 'badge';
                            }
                            return \kartik\helpers\Html::badge($model['que_status_name'], ['class' => $badgeClass, 'style' => 'width: 160px;']);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'status_name',
                        'value' => function ($model, $key, $index, $column) {
                            return $model['que_status_name'];
                        }
                    ],
                    [
                        'class' => ActionTable::className(),
                        'template' => '<div class="btn-group" role="group">{qrcode} {print} {update} {delete}</div>',
                        'updateOptions' => [
                            'role' => 'modal-remote',
                            'class' => 'btn btn-default text-info',
                            'title' => 'แก้ไข',
                        ],
                        'deleteOptions' => [
                            'class' => 'btn btn-default text-danger activity-delete',
                            'title' => 'ลบ',
                        ],
                        'buttons' => [
                            'qrcode' => function ($url, $model, $key) {
                                return Html::a(Icon::show('qrcode', [], Icon::BSG), $url, ['target' => '_blank', 'title' => 'QRCode', 'class' => 'btn btn-default']);
                            },
                            'print' => function ($url, $model, $key) {
                                return Html::a(Icon::show('print', [], Icon::BSG), $url, ['target' => '_blank', 'title' => 'พิมพ์บัตรคิว', 'class' => 'btn btn-default']);
                            },
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action == 'qrcode') {
                                return Url::to(['/qrcode/mobile-view', 'id' => $key]);
                            }
                            if ($action == 'print') {
                                return Url::to(['/app/kiosk/print-ticket', 'que_ids' => $key]);
                            }
                            if ($action == 'update') {
                                return Url::to(['/app/kiosk/update-queue', 'id' => $key]);
                            }
                            if ($action == 'delete') {
                                return Url::to(['/app/kiosk/delete-queue', 'id' => $key]);
                            }
                        },
                    ],
                ],
            ]);
            return ['data' => $columns->renderDataColumns()];
        } else {
            throw  new MethodNotAllowedHttpException('method not allowed.');
        }
    }

    private function getBadgeStatus($status)
    {
        if ($status == 0) {
            return \kartik\helpers\Html::badge(Icon::show('close') . ' ปิดใช้งาน', ['class' => 'badge badge-danger']);
        } elseif ($status == 1) {
            return \kartik\helpers\Html::badge(Icon::show('check') . ' เปิดใช้งาน', ['class' => 'badge badge-success']);
        }
    }
}