<?php

namespace common\modules\app\models;

use common\components\AutoNumber;
use common\modules\app\traits\ModelTrait;
use homer\behaviors\CoreMultiValueBehavior;
use homer\helpers\ArrayHelper;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Json;
use yii\mongodb\ActiveRecord;

/**
 * This is the model class for collection "tb_queue".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $queue_no
 * @property mixed $queue_hn
 * @property mixed $pt_name
 * @property mixed $service_id
 * @property mixed $service_group_id
 * @property mixed $created_at
 * @property mixed $updated_at
 * @property mixed $created_by
 * @property mixed $updated_by
 * @property mixed $queue_status_id
 * @property mixed $wait_time
 * @property mixed $status_times
 */
class TbQueue extends \yii\mongodb\ActiveRecord
{
    use ModelTrait;

    const STATUS_WAIT = 1;
    const STATUS_CALL = 2;
    const STATUS_HOLD = 3;
    const STATUS_SUCCESS = 4;

    /**
     * {@inheritdoc}
     */
    public static function collectionName()
    {
        return 'tb_queue';
    }

    /**
     * {@inheritdoc}
     */
    public function attributes()
    {
        return [
            '_id',
            'queue_no',
            'queue_hn',
            'pt_name',
            'service_id',
            'service_group_id',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
            'queue_status_id',
            'wait_time',
            'status_times',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                'value' => (string)Yii::$app->user->id,
            ],
            [
                'class' => CoreMultiValueBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'queue_no',
                ],
                'value' => function ($event) {
                    if (empty($this->queue_no)) {
                        return $this->generateQnumber();
                    } else {
                        return $event->sender[$event->data];
                    }
                },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['queue_hn'], 'required', 'on' => ['ticket', 'create']],
            [['service_id', 'service_group_id', 'queue_status_id', 'pt_name'], 'required', 'on' => 'create'],
            [['created_by', 'updated_by', 'queue_status_id'], 'integer'],
            [['created_at', 'updated_at', 'search_by', 'wait_time', 'status_times'], 'safe'],
            [['queue_no', 'queue_hn', 'service_id', 'service_group_id', 'pt_name'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'queue_no' => 'Queue No',
            'queue_hn' => 'Queue Hn',
            'pt_name' => 'Pt Name',
            'service_id' => 'Service ID',
            'service_group_id' => 'Service Group ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'queue_status_id' => 'Que Status ID',
            'wait_time' => 'Wait Time',
            'status_times' => 'Status Times',
        ];
    }

    public function generateQnumber()
    {
        $service = $this->findModelService($this->service_id);
        $queueArray = ArrayHelper::map($this->find()->where(['service_id' => $this->service_id])->all(), '_id', 'queue_no');
        $queueNo = [];
        $max = null;
        $qid = null;
        if (count($queueArray) > 0) {
            foreach ($queueArray as $key => $no) {
                $queueNo[(string)$key] = preg_replace("/[^0-9\.]/", '', $no);
            }
            $max = max($queueNo);
            $qid = array_search($max, $queueNo);
        }
        $component = \Yii::createObject([
            'class' => AutoNumber::className(),
            'prefix' => $service ? $service['service_prefix'] : 'A',
            'number' => ArrayHelper::getValue($queueArray, $qid, null),
            'digit' => $service ? $service['service_numdigit'] : 3,
        ]);
        return $component->generate();
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if (!$this->isNewRecord && !empty($this->status_times)) {
            $status_times = $this->status_times;
            $keys = ArrayHelper::getColumn($status_times, 'id');
            if (ArrayHelper::isIn($this->queue_status_id, $keys)) {
                foreach ($status_times as $key => $status_time) {
                    if ($status_time['id'] == $this->queue_status_id) {
                        $status_times[$key]['time'] = \Yii::$app->formatter->asDate('now', 'php:H:i:s');
                        break;
                    }
                }
                $this->status_times = $status_times;
            } else {
                $status_times = ArrayHelper::merge($status_times, [
                    [
                        'id' => $this->queue_status_id,
                        'time' => \Yii::$app->formatter->asDate('now', 'php:H:i:s'),
                    ]
                ]);
                $this->status_times = $status_times;
            }
        } else {
            $this->status_times = [
                [
                    'id' => $this->queue_status_id,
                    'time' => \Yii::$app->formatter->asDate('now', 'php:H:i:s'),
                ]
            ];
        }
        return true;
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['ticket'] = ['queue_hn'];
        $scenarios['create'] = ['service_id', 'service_group_id', 'queue_status_id', 'queue_hn', 'pt_name', 'queue_no'];
        return $scenarios;
    }
}
