<?php

namespace common\modules\app\models;

use Yii;

/**
 * This is the model class for collection "tb_service_group".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $service_group_name
 * @property mixed $service_group_status
 */
class TbServiceGroup extends \yii\mongodb\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function collectionName()
    {
        return 'tb_service_group';
    }

    /**
     * {@inheritdoc}
     */
    public function attributes()
    {
        return [
            '_id',
            'service_group_name',
            'service_group_status',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['service_group_name', 'service_group_status'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'รหัสกลุ่มบริการ',
            'service_group_name' => 'ชื่อกลุ่มบริการ',
            'service_group_status' => 'สถานะ',
        ];
    }
}
