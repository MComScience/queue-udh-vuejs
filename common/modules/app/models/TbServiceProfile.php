<?php

namespace common\modules\app\models;

use Yii;

/**
 * This is the model class for collection "tb_service_profile".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $service_profile_name
 * @property mixed $counter_service_type_id
 * @property mixed $service_id
 * @property mixed $service_profile_status
 */
class TbServiceProfile extends \yii\mongodb\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function collectionName()
    {
        return 'tb_service_profile';
    }

    /**
     * {@inheritdoc}
     */
    public function attributes()
    {
        return [
            '_id',
            'service_profile_name',
            'counter_service_type_id',
            'service_id',
            'service_profile_status',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['service_profile_name', 'counter_service_type_id', 'service_id', 'service_profile_status'], 'required'],
            [['service_profile_status'], 'integer'],
            [['service_id'], 'safe'],
            [['service_profile_name', 'counter_service_type_id'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'service_profile_name' => 'ชื่อโปรไฟล์',
            'counter_service_type_id' => 'เคาท์เตอร์',
            'service_id' => 'จุดบริการ',
            'service_profile_status' => 'สถานะ',
        ];
    }
}
