<?php

namespace common\modules\app\models;

use Yii;

/**
 * This is the model class for collection "tb_service".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $service_name
 * @property mixed $service_group_id
 * @property mixed $print_template_id
 * @property mixed $print_copy_qty
 * @property mixed $service_prefix
 * @property mixed $service_numdigit
 * @property mixed $service_status
 * @property mixed $avg_time
 * @property mixed $avg_time_more
 */
class TbService extends \yii\mongodb\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function collectionName()
    {
        return 'tb_service';
    }

    /**
     * {@inheritdoc}
     */
    public function attributes()
    {
        return [
            '_id',
            'service_name',
            'service_group_id',
            'print_template_id',
            'print_copy_qty',
            'service_prefix',
            'service_numdigit',
            'service_status',
            'avg_time',
            'avg_time_more',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['service_name', 'print_template_id', 'service_prefix', 'service_numdigit', 'print_copy_qty', 'service_status'], 'required'],
            [['print_copy_qty', 'service_numdigit', 'avg_time', 'avg_time_more', 'service_status'], 'integer'],
            [['service_name'], 'string', 'max' => 255],
            [['service_group_id', 'print_template_id'], 'string'],
            [['service_prefix'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'service_name' => 'ชื่อบริการ',
            'service_group_id' => 'รหัสกลุ่มบริการ',
            'print_template_id' => 'แบบการพิมพ์บัตรคิว',
            'print_copy_qty' => 'จำนวนพิมพ์/ครั้ง',
            'service_prefix' => 'ตัวอักษร/ตัวเลข นำหน้าคิว',
            'service_numdigit' => 'จำนวนหลักหมายเลขคิว',
            'service_status' => 'สถานะ',
            'avg_time' => 'เวลารอ(นาที)',
            'avg_time_more' => 'เวลารอเผื่อ(นาที)',
        ];
    }
}
