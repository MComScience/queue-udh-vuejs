<?php

namespace common\modules\app\models;

use Yii;

/**
 * This is the model class for collection "tb_counter_service".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $counter_service_name
 * @property mixed $counter_service_call_number
 * @property mixed $counter_service_type_id
 * @property mixed $service_group_id
 * @property mixed $sound_number_id
 * @property mixed $sound_service_id
 * @property mixed $counter_service_status
 */
class TbCounterService extends \yii\mongodb\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function collectionName()
    {
        return 'tb_counter_service';
    }

    /**
     * {@inheritdoc}
     */
    public function attributes()
    {
        return [
            '_id',
            'counter_service_name',
            'counter_service_call_number',
            'counter_service_type_id',
            'service_group_id',
            'sound_number_id',
            'sound_service_id',
            'counter_service_status',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['counter_service_name', 'counter_service_call_number', 'sound_service_id', 'counter_service_status'], 'required'],
            [['counter_service_call_number', 'counter_service_status'], 'integer'],
            [['counter_service_name','sound_number_id', 'sound_service_id','service_group_id', 'counter_service_type_id'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'counter_service_name' => 'ชื่อจุดบริการ',
            'counter_service_call_number' => 'หมายเลข',
            'counter_service_type_id' => 'ประเภทบริการ',
            'service_group_id' => 'กลุ่มบริการ',
            'sound_number_id' => 'เสียงเรียกหมายเลข',
            'sound_service_id' => 'เสียงเรียกบริการ',
            'counter_service_status' => 'สถานะ',
        ];
    }
}
