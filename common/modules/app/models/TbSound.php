<?php

namespace common\modules\app\models;

use Yii;

/**
 * This is the model class for collection "tb_sound".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $sound_name
 * @property mixed $sound_path
 * @property mixed $sound_label
 * @property mixed $sound_type
 */
class TbSound extends \yii\mongodb\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function collectionName()
    {
        return 'tb_sound';
    }

    /**
     * {@inheritdoc}
     */
    public function attributes()
    {
        return [
            '_id',
            'sound_name',
            'sound_path',
            'sound_label',
            'sound_type',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sound_name', 'sound_path', 'sound_label', 'sound_type'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'sound_name' => 'Sound Name',
            'sound_path' => 'Sound Path',
            'sound_label' => 'Sound Label',
            'sound_type' => 'Sound Type',
        ];
    }
}
