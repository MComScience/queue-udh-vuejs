<?php

namespace common\modules\app\models;

use homer\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for collection "tb_display".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $display_name
 * @property mixed $page_length
 * @property mixed $text_th_left
 * @property mixed $text_th_right
 * @property mixed $text_hold
 * @property mixed $display_css
 * @property mixed $background_color
 * @property mixed $display_status
 * @property mixed $counter_service_id
 * @property mixed $service_id
 * @property mixed $que_column_length
 * @property mixed $show_que_hold
 * @property mixed $text_top_left
 * @property mixed $text_top_center
 * @property mixed $text_top_right
 * @property mixed $text_th_lastq_left
 * @property mixed $text_th_lastq_right
 */
class TbDisplay extends \yii\mongodb\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function collectionName()
    {
        return 'tb_display';
    }

    /**
     * {@inheritdoc}
     */
    public function attributes()
    {
        return [
            '_id',
            'display_name',
            'page_length',
            'text_th_left',
            'text_th_right',
            'text_hold',
            'display_css',
            'background_color',
            'display_status',
            'counter_service_id',
            'service_id',
            'que_column_length',
            'show_que_hold',
            'text_top_left',
            'text_top_center',
            'text_top_right',
            'text_th_lastq_left',
            'text_th_lastq_right',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['display_name', 'page_length', 'text_th_left', 'text_th_right', 'text_hold', 'display_css', 'background_color', 'display_status', 'counter_service_id', 'service_id', 'que_column_length', 'show_que_hold', 'text_top_left', 'text_top_center', 'text_top_right', 'text_th_lastq_left', 'text_th_lastq_right'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'display_name' => 'ชื่อจอแสดงผล',
            'page_length' => 'จำนวนแถวที่แสดง',
            'text_th_left' => 'ข้อความส่วนหัวตาราง (ซ้าย)',
            'text_th_right' => 'ข้อความส่วนหัวตาราง (ขวา)',
            'text_hold' => 'ข้อความตารางพักคิว',
            'display_css' => 'Display Css',
            'background_color' => 'สีพื้นหลัง',
            'display_status' => 'สถานะ',
            'counter_service_id' => 'เคาท์เตอร์',
            'service_id' => 'จุดบริการ',
            'que_column_length' => 'จำนวนเลขคิวที่แสดง/แถว',
            'show_que_hold' => 'แสดงคิวที่เรียกไปแล้ว',
            'text_top_left' => 'ข้อความ (ซ้าย)',
            'text_top_center' => 'ข้อความ (กลาง)',
            'text_top_right' => 'ข้อความ (ขวา)',
            'text_th_lastq_left' => 'ข้อความคิวล่าสุด (ซ้าย)',
            'text_th_lastq_right' => 'ข้อความคิวล่าสุด (ขวา)',
        ];
    }

    public function getDefaultCss(){
        return '<pre class=" css" data-pbcklang="css" data-pbcktabsize="4">
        /*สีพื้นหลัง*/
        body {
            background-color: #204d74;
        }
        table#tb-display thead tr th.th-right {
            border-top: 5px solid #ffffff !important;
            border-bottom: 5px solid #ffffff !important;
            border-right: 5px solid #ffffff !important;
            border-top-right-radius: 10px;
            border-bottom-right-radius: 10px;
            /*font-size: 40px !important;*/
        }
        
        table#tb-display thead tr th.th-left {
            border-top: 5px solid #ffffff !important;
            border-bottom: 5px solid #ffffff !important;
            border-left: 5px solid #ffffff !important;
            border-top-left-radius: 10px;
            border-bottom-left-radius: 10px;
            /*font-size: 40px !important;*/
        }
        
        table#tb-display tbody tr td.td-left {
            border-top: 5px solid #ffffff !important;
            border-bottom: 5px solid #ffffff !important;
            border-left: 5px solid #ffffff !important;
            border-top-left-radius: 10px;
            border-bottom-left-radius: 10px;
            /*font-size: 40px !important;*/
        }
        
        table#tb-display tbody tr td.td-right {
            border-top: 5px solid #ffffff !important;
            border-bottom: 5px solid #ffffff !important;
            border-right: 5px solid #ffffff !important;
            border-top-right-radius: 10px;
            border-bottom-right-radius: 10px;
            /*font-size: 40px !important;*/
        }
        /*background-color = สีพื้นหลังตารางส่วนหัว*/
        /*color = สีตัวอักษร*/
        table#tb-display thead tr {
            width: 50%;
            border-radius: 15px;
            border: 5px solid white;
            background-color: #000000;
            color: #ffffff;
            font-weight: bold;
        }
        /*background-color = สีพื้นหลังแถว*/
        /*color = สีตัวอักษร*/
        table#tb-display tbody tr {
            width: 50%;
            border-radius: 15px;
            border: 5px solid white;
            background-color: #666666;
            color: #ffffff;
            font-weight: bold;
        }
        table#tb-hold tbody tr td.td-left{
            width: 50%;
            border-top: 5px solid #ffffff !important;
            border-bottom: 5px solid #ffffff !important;
            border-right: 5px solid #ffffff !important;
            border-left: 5px solid #ffffff !important;
            border-top-right-radius: 10px;
            border-bottom-right-radius: 10px;
            border-top-left-radius: 10px;
            border-bottom-left-radius: 10px;
            background-color: #666666;
            color: #ffffff;
            vertical-align: middle;
            /*font-size: 40px !important;*/
        }
        
        table#tb-hold tbody tr td.td-right{
            width: 50%;
            border-top: 0px solid white !important;
            color: yellow;
            font-weight: bold;
            vertical-align: middle;
            /*font-size: 40px !important;*/
        }
        /* คิวล่าสุด */
        table#tb-lastque thead tr th.th-right {
            border-top: 5px solid #ffffff !important;
            border-bottom: 5px solid #ffffff !important;
            border-right: 5px solid #ffffff !important;
            border-top-right-radius: 10px;
            border-bottom-right-radius: 10px;
            /*font-size: 40px !important;*/
        }
        
        table#tb-lastque thead tr th.th-left {
            border-top: 5px solid #ffffff !important;
            border-bottom: 5px solid #ffffff !important;
            border-left: 5px solid #ffffff !important;
            border-top-left-radius: 10px;
            border-bottom-left-radius: 10px;
            /*font-size: 40px !important;*/
        }
        table#tb-lastque tbody tr td.td-left {
            border-top: 5px solid #ffffff !important;
            border-bottom: 5px solid #ffffff !important;
            border-left: 5px solid #ffffff !important;
            border-top-left-radius: 10px;
            border-bottom-left-radius: 10px;
            /*font-size: 40px !important;*/
        }
        
        table#tb-lastque tbody tr td.td-right {
            border-top: 5px solid #ffffff !important;
            border-bottom: 5px solid #ffffff !important;
            border-right: 5px solid #ffffff !important;
            border-top-right-radius: 10px;
            border-bottom-right-radius: 10px;
            /*font-size: 40px !important;*/
        }
        /*color = สีตัวอักษร*/
        table#tb-lastque thead tr {
            width: 50%;
            border-radius: 15px;
            border: 5px solid white;
            background-color: #000000;
            color: #ffffff;
            font-weight: bold;
        }
        table#tb-lastque tbody tr {
            width: 50%;
            border-radius: 15px;
            border: 5px solid white;
            background-color: #666666;
            color: #ffffff;
            font-weight: bold;
        }</pre>';
    }

    public function getCounterServiceData(){
        $query = TbCounterServiceType::find()->all();
        return ArrayHelper::map($query,'_id','counter_service_type_name');
    }

    public function getServiceData(){
        $services = TbService::find()->where(['service_status' => '1'])->all();
        $options = [];
        foreach ($services as $service){
            $serviceGroup = TbServiceGroup::findOne($service['service_group_id']);
            $options[] = [
                'service_id' => (string)$service['_id'],
                'service_name' => $serviceGroup['service_group_name'].' : '.$service['service_prefix'].' : '.$service['service_name']
            ];
        }
        return ArrayHelper::map($options,'service_id','service_name');
    }
}
