<?php

namespace common\modules\app\models;

use Yii;

/**
 * This is the model class for collection "tb_counter_service_type".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $counter_service_type_name
 */
class TbCounterServiceType extends \yii\mongodb\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function collectionName()
    {
        return 'tb_counter_service_type';
    }

    /**
     * {@inheritdoc}
     */
    public function attributes()
    {
        return [
            '_id',
            'counter_service_type_name',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['counter_service_type_name'], 'required'],
            [['counter_service_type_name'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'counter_service_type_name' => 'ชื่อประเภทบริการ',
        ];
    }
}
