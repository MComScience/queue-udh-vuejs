<?php

namespace common\modules\app\models;

use Yii;

/**
 * This is the model class for collection "tb_sound_station".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $sound_station_name
 * @property mixed $counter_service_id
 * @property mixed $sound_station_status
 */
class TbSoundStation extends \yii\mongodb\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function collectionName()
    {
        return 'tb_sound_station';
    }

    /**
     * {@inheritdoc}
     */
    public function attributes()
    {
        return [
            '_id',
            'sound_station_name',
            'counter_service_id',
            'sound_station_status',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sound_station_name', 'counter_service_id', 'sound_station_status'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'sound_station_name' => 'Sound Station Name',
            'counter_service_id' => 'Counter Service ID',
            'sound_station_status' => 'Sound Station Status',
        ];
    }
}
