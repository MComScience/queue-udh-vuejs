<?php

namespace common\modules\app\models;

use Yii;

/**
 * This is the model class for collection "tb_queue_status".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $que_status_name
 */
class TbQueueStatus extends \yii\mongodb\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function collectionName()
    {
        return 'tb_queue_status';
    }

    /**
     * {@inheritdoc}
     */
    public function attributes()
    {
        return [
            '_id',
            'que_status_name',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['que_status_name'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'que_status_name' => 'Que Status Name',
        ];
    }
}
