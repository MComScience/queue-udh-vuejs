<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 25/2/2562
 * Time: 12:09
 */
namespace common\modules\app\traits;

use common\modules\app\models\TbCounterServiceType;
use common\modules\app\models\TbDisplay;
use common\modules\app\models\TbQueue;
use common\modules\app\models\TbService;
use common\modules\app\models\TbServiceGroup;
use common\modules\app\models\TbServiceProfile;
use common\modules\app\models\TbSoundStation;
use common\modules\app\models\TbTicket;
use yii\web\NotFoundHttpException;

trait ModelTrait
{
    protected function findModelServiceGroup($id)
    {
        if (($model = TbServiceGroup::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelTicket($id)
    {
        if (($model = TbTicket::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelCounterServiceType($id)
    {
        if (($model = TbCounterServiceType::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelSoundStation($id)
    {
        if (($model = TbSoundStation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelServiceProfile($id)
    {
        if (($model = TbServiceProfile::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelDisplay($id)
    {
        if (($model = TbDisplay::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelService($id)
    {
        if (($model = TbService::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelQueue($id)
    {
        if (($model = TbQueue::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}