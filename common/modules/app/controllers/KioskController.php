<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 26/2/2562
 * Time: 10:36
 */

namespace common\modules\app\controllers;

use common\modules\app\models\TbQueue;
use common\modules\app\models\TbService;
use common\modules\app\models\TbServiceGroup;
use common\modules\app\traits\ModelTrait;
use homer\helpers\ArrayHelper;
use Yii;
use common\components\AccessRule;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\MethodNotAllowedHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class KioskController extends Controller
{
    use ModelTrait;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete-queue' => ['post']
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className()
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            '@'
                        ],
                    ],
                ],
            ]
        ];
    }

    public function actionIndex()
    {
        $serviceGroups = TbServiceGroup::find()->all();
        return $this->render('index', [
            'serviceGroups' => $serviceGroups
        ]);
    }

    public function actionIssueCard($id)
    {
        $model = $this->findModelServiceGroup($id);
        return $this->render('issue-card', [
            'model' => $model
        ]);
    }

    public function actionDataServices($id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $services = TbService::find()->where(['service_group_id' => $id, 'service_status' => '1'])->all();
        return $services;
    }

    public function actionSearchData()
    {
        $request = Yii::$app->request;
        $modelQueue = new TbQueue();
        $modelQueue->scenario = 'ticket';

        $personal = null;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $searchData = $request->post();
            $queue_hn = str_replace(" ", "", $searchData['que_hn']);
            $search_by = isset($searchData['search_by']) ? $searchData['search_by'] : null;
            if (empty($queue_hn)) {
                return [
                    'success' => false,
                    'message' => 'ไม่พบข้อมูล',
                ];
            }
            $cache = Yii::$app->cache;
            $keyCache = 'QueueData';
            $cacheData = $cache->get($keyCache);
            // ถ้าไม่มีแคช
            if ($cacheData === false) {
                $dataSearch = $this->fetchData();
                //ถ้าไม่มีข้อมูล
                if (empty($dataSearch)) {
                    return [
                        'success' => false,
                        'message' => 'ไม่พบข้อมูล!',
                        'personal' => $personal,
                        'form' => 'cache'
                    ];
                }
                //ถ้ามีข้อมูล
                $cache->set($keyCache, $dataSearch);
                $personal = $this->getDataSearch($dataSearch, $search_by, $queue_hn);
            } else {// ถ้ามีแคช ให้หาในแคช
                $personal = $this->getDataSearch($cacheData, $search_by, $queue_hn);
            }

            //ถ้าหาข้อมูลแล้วไม่พบข้อมูล ให้หาใหม่อีกรอบ
            if (empty($personal)) {
                $cache->delete($keyCache);
                $dataSearch = $this->fetchData();
                if (empty($dataSearch)) {
                    return [
                        'success' => false,
                        'message' => 'ไม่พบข้อมูล!',
                        'personal' => $personal,
                        'form' => 'query'
                    ];
                }
                //ถ้ามีข้อมูล
                $cache->set($keyCache, $dataSearch);
                $personal = $this->getDataSearch($dataSearch, $search_by, $queue_hn);
            }

            if (!empty($personal)) {
                $modelService = $this->findModelService($searchData['service_id']);
                $modelServiceGroup = $this->findModelServiceGroup($searchData['service_group_id']);
                return [
                    'success' => true,
                    'message' => 'success',
                    'personal' => $personal,
                    'modelService' => $modelService,
                    'modelServiceGroup' => $modelServiceGroup,
                ];
            } else {
                return [
                    'success' => false,
                    'message' => 'ไม่พบข้อมูล!',
                    'personal' => $personal,
                    'form' => 'other'
                ];
            }
        }
    }

    public function actionSaveSearch()
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $personal = $request->post('personal');
            $modelService = $request->post('modelService');
            $modelServiceGroup = $request->post('modelServiceGroup');

            $modelQue = new TbQueue();
            $modelQue->scenario = 'create';
            $modelQue->setAttributes([
                'service_id' => $modelService['_id'],
                'service_group_id' => $modelServiceGroup['_id'],
                'pt_name' => $personal['name'],
                'queue_hn' => str_replace(" ", "", $personal['hn']),
                'queue_status_id' => TbQueue::STATUS_WAIT
            ]);
            if ($modelQue->save()) {
                return [
                    'success' => true,
                    'modelQue' => $modelQue,
                    'personal' => $personal,
                    'modelService' => $modelService,
                    'modelServiceGroup' => $modelServiceGroup,
                    'url' => Url::to(['/app/kiosk/print-ticket', 'que_id' => (string)$modelQue['_id']])
                ];
            } else {
                throw new HttpException(422, Json::encode($modelQue->errors));
            }
        } else {
            throw new BadRequestHttpException(Yii::t('app', 'The system could not process your request. Please check and try again.'));
        }
    }

    private function fetchData()
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('GET')
            ->setUrl('http://202.80.234.74/site/get-data-table')
            ->send();
        if ($response->isOk) {
            return $response->getData();
        }
        return false;
    }

    private function getDataSearch($dataSearch, $search_by, $queue_hn)
    {
        $mapCardId = static::map($dataSearch, 'CardID', 'name');
        $mapHn = static::map($dataSearch, 'hn', 'name');
        if ($search_by == '1') { //รหัส ปชช
            return ArrayHelper::getValue($mapCardId, $queue_hn, null);
        } elseif ($search_by == '0') {// HN
            return ArrayHelper::getValue($mapHn, $queue_hn, null);
        }
    }

    public static function map($array, $from, $to, $group = null)
    {
        $result = [];
        foreach ($array as $element) {
            $key = ArrayHelper::getValue($element, $from);
            $value = ArrayHelper::getValue($element, $to);
            if ($group !== null) {
                $result[ArrayHelper::getValue($element, $group)][$key] = $value;
            } else {
                $result[str_replace(" ", "", $key)] = $element;
            }
        }

        return $result;
    }

    public function actionDeleteQueue($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModelQueue($id);
        $model->delete();

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true];
        } else {
            return $this->redirect(['ticket']);
        }
    }

    public function actionUpdateQueue($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModelQueue($id);
        $model->scenario = 'create';
        $oldservice = $model['service_id'];

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "แก้ไขรายการคิว #" . $model['queue_no'],
                    'content' => $this->renderAjax('_form_queue', [
                        'model' => $model,
                    ]),
                    'footer' => '',
                ];
            } else if ($model->load($request->post())) {
                $data = $request->post($model->formName());
                if ($oldservice != $data['service_id']) {
                    $model->queue_no = $model->generateQnumber();
                }
                if ($model->save()) {
                    return [
                        'title' => "แก้ไขรายการคิว #" . $model['queue_no'],
                        'content' => '<span class="text-success">บันทึกสำเร็จ!</span>',
                        'footer' => Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]),
                        'status' => '200',
                    ];
                } else {
                    return [
                        'title' => "แก้ไขรายการคิว #" . $model['queue_no'],
                        'content' => $this->renderAjax('_form_queue', [
                            'model' => $model,
                        ]),
                        'footer' => '',
                        'status' => 'validate',
                        'validate' => ActiveForm::validate($model),
                    ];
                }
            } else {
                return [
                    'title' => "แก้ไขรายการคิว #" . $model['queue_no'],
                    'content' => $this->renderAjax('_form_que', [
                        'model' => $model,
                    ]),
                    'footer' => '',
                    'status' => 'validate',
                    'validate' => ActiveForm::validate($model),
                ];
            }
        } else {
            throw new MethodNotAllowedHttpException('method not allowed.');
        }
    }

    public function actionChildServiceGroup()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            $list = TbService::find()->andWhere(['service_group_id' => $id])->asArray()->all();
            $selected = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $data) {
                    $out[] = ['id' => (string)$data['_id'], 'name' => $data['service_name']];
                    if ($i == 0) {
                        $selected = (string)$data['_id'];
                    }
                }
                // Shows how you can preselect a value
                echo Json::encode(['output' => $out, 'selected' => $selected]);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }
}