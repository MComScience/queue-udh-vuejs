<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 24/2/2562
 * Time: 16:47
 */
namespace common\modules\app\controllers;

use app\models\MultipleModel;
use common\components\AccessRule;
use common\modules\app\models\TbCounterService;
use common\modules\app\models\TbCounterServiceType;
use common\modules\app\models\TbDisplay;
use common\modules\app\models\TbService;
use common\modules\app\models\TbServiceGroup;
use common\modules\app\models\TbServiceProfile;
use common\modules\app\models\TbSound;
use common\modules\app\models\TbSoundStation;
use common\modules\app\models\TbTicket;
use common\modules\app\traits\ModelTrait;
use mcomscience\sweetalert2\SweetAlert2;
use trntv\filekit\actions\DeleteAction;
use trntv\filekit\actions\UploadAction;
use Yii;
use yii\base\Model;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use homer\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\mongodb\Exception;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\MethodNotAllowedHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class SettingsController extends Controller
{
    use ModelTrait;
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete-service-group' => ['POST'],
                    'delete-ticket' => ['POST'],
                    'delete-sound-station' => ['POST'],
                    'delete-service-profile' => ['POST'],
                    'delete-display' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className()
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            '@'
                        ],
                    ],
                ],
            ]
        ];
    }

    public function actions()
    {
        return [
            'file-upload' => [
                'class' => UploadAction::className(),
                'deleteRoute' => 'file-delete',
            ],
            'file-delete' => [
                'class' => DeleteAction::className()
            ]
        ];
    }

    #กลุ่มบริการ
    public function actionServiceGroup()
    {
        return $this->render('_content_service_group');
    }

    public function actionDeleteServiceGroup($id, $service_id = null)
    {
        $request = Yii::$app->request;
        if ($service_id != null) {
            TbService::findOne(['_id' => $service_id])->delete();
        }
        if (TbService::find()->where(['_id' => $id])->count() == 0) {
            TbServiceGroup::findOne(['_id' => $id])->delete();
        }

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->redirect(['service-group']);
        }
    }

    public function actionCreateServiceGroup()
    {
        $request = Yii::$app->request;
        $model = new TbServiceGroup();
        $modelServices = [new TbService()];
        $title = 'บันทึกกลุ่มบริการ';

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => $title,
                    'content' => $this->renderAjax('_form_service_group', [
                        'model' => $model,
                        'modelServices' => (empty($modelServices)) ? [new TbService()] : $modelServices,
                    ]),
                    'footer' => '',
                ];
            } elseif ($model->load($request->post())) {
                $attrId = '_id';
                $oldIDs = ArrayHelper::map($modelServices, $attrId, $attrId);
                $modelServices = MultipleModel::createMultiple(TbService::classname(), $modelServices, $attrId);
                MultipleModel::loadMultiple($modelServices, $request->post());
                $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelServices, $attrId, $attrId)));

                // validate all models
                $valid = $model->validate();
                $valid = Model::validateMultiple($modelServices) && $valid;
                if ($valid) {
                    try {
                        $model->service_group_status = 1; // default value
                        if ($flag = $model->save(false)) {
                            if (!empty($deletedIDs) && is_array($deletedIDs)) {
                                foreach ($deletedIDs as $_id){
                                    if(!empty($_id)){
                                        TbService::findOne((string)$_id)->delete();
                                    }
                                }
                            }
                            foreach ($modelServices as $modelService) {
                                $modelService->service_group_id = (string)$model['_id'];
                                if (!($flag = $modelService->save(false))) {
                                    break;
                                }
                            }
                        }
                        if ($flag) {
                            return [
                                'title' => $title,
                                'content' => '<span class="text-success">บันทึกสำเร็จ!</span>',
                                'footer' => Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]),
                                'status' => 200,
                            ];
                        }
                    } catch (\Exception $e) {
                        throw new Exception($e->getMessage());
                    }
                } else {
                    return [
                        'title' => $title,
                        'content' => $this->renderAjax('_form_service_group', [
                            'model' => $model,
                            'modelServices' => (empty($modelServices)) ? [new TbService()] : $modelServices,
                        ]),
                        'footer' => '',
                        'validate' => ArrayHelper::merge(ActiveForm::validateMultiple($modelServices), ActiveForm::validate($model)),
                        'status' => 422,
                    ];
                }
            } else {
                return [
                    'title' => $title,
                    'content' => $this->renderAjax('_form_service_group', [
                        'model' => $model,
                        'modelServices' => (empty($modelServices)) ? [new TbService()] : $modelServices,
                    ]),
                    'footer' => '',
                    'validate' => ArrayHelper::merge(ActiveForm::validateMultiple($modelServices), ActiveForm::validate($model)),
                    'status' => 422,
                ];
            }
        } else {
            throw new MethodNotAllowedHttpException('method not allowed.');
        }
    }

    public function actionUpdateServiceGroup($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModelServiceGroup($id);
        $modelServices = TbService::find()->where(['service_group_id' => $id])->all();
        $title = 'บันทึกกลุ่มบริการ';

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => $title,
                    'content' => $this->renderAjax('_form_service_group', [
                        'model' => $model,
                        'modelServices' => (empty($modelServices)) ? [new TbService()] : $modelServices,
                    ]),
                    'footer' => '',
                ];
            } elseif ($model->load($request->post())) {
                $attrId = '_id';
                $oldIDs = ArrayHelper::map($modelServices, $attrId, $attrId);
                $modelServices = MultipleModel::createMultiple(TbService::classname(), $modelServices, $attrId);
                MultipleModel::loadMultiple($modelServices, $request->post());
                $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelServices, $attrId, $attrId)));
                // validate all models
                $valid = $model->validate();
                $valid = Model::validateMultiple($modelServices) && $valid;
                if ($valid) {
                    try {
                        if ($flag = $model->save(false)) {
                            if (!empty($deletedIDs) && is_array($deletedIDs)) {
                                foreach ($deletedIDs as $_id){
                                    if(!empty($_id)){
                                        TbService::findOne((string)$_id)->delete();
                                    }
                                }
                            }
                            foreach ($modelServices as $modelService) {
                                $modelService->service_group_id = (string)$model['_id'];
                                if (!($flag = $modelService->save(false))) {
                                    break;
                                }
                            }
                        }
                        if ($flag) {
                            return [
                                'title' => $title,
                                'content' => '<span class="text-success">บันทึกสำเร็จ!</span>',
                                'footer' => Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]),
                                'status' => 200,
                            ];
                        }
                    } catch (\Exception $e) {
                        throw new Exception($e->getMessage());
                    }
                } else {
                    return [
                        'title' => $title,
                        'content' => $this->renderAjax('_form_service_group', [
                            'model' => $model,
                            'modelServices' => (empty($modelServices)) ? [new TbService()] : $modelServices,
                        ]),
                        'footer' => '',
                        'validate' => ArrayHelper::merge(ActiveForm::validateMultiple($modelServices), ActiveForm::validate($model)),
                        'status' => 422,
                    ];
                }
            } else {
                return [
                    'title' => $title,
                    'content' => $this->renderAjax('_form_service_group', [
                        'model' => $model,
                        'modelServices' => (empty($modelServices)) ? [new TbService()] : $modelServices,
                    ]),
                    'footer' => '',
                    'validate' => ArrayHelper::merge(ActiveForm::validateMultiple($modelServices), ActiveForm::validate($model)),
                    'status' => 422,
                ];
            }
        } else {
            throw new MethodNotAllowedHttpException('method not allowed.');
        }
    }

    #บัตรคิว
    public function actionTicket()
    {
        return $this->render('_content_ticket');
    }

    public function actionDeleteTicket($id)
    {
        $request = Yii::$app->request;
        TbTicket::findOne($id)->delete();

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->redirect(['ticket']);
        }
    }

    public function actionCreateTicket()
    {
        $request = Yii::$app->request;
        $model = new TbTicket();
        $data = $request->post($model->formName());
        if ($request->isPost){
            $model->hos_name_th = isset($data['hos_name_th']) ? $data['hos_name_th'] : '';
            $model->hos_name_en = isset($data['hos_name_en']) ? $data['hos_name_en'] : '';
            $model->template = isset($data['template']) ? $data['template'] : '';
            $model->ticket_status = isset($data['ticket_status']) ? (int)$data['ticket_status'] : (int)1;
        }

        if ($model->load($request->post()) && $model->save()) {
            Yii::$app->session->setFlash(SweetAlert2::TYPE_SUCCESS, [
                [
                    'title' => 'Successfully',
                    'text' => 'บันทึกสำเร็จ',
                    'timer' => 2000,
                    'showConfirmButton' => false
                ],
            ]);
            return $this->redirect(['update-ticket', 'id' => (string)$model['_id']]);
        } else {
            return $this->render('_form_ticket', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdateTicket($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModelTicket($id);
        $data = $request->post($model->formName());
        if ($request->isPost){
            $model->hos_name_th = isset($data['hos_name_th']) ? $data['hos_name_th'] : '';
            $model->hos_name_en = isset($data['hos_name_en']) ? $data['hos_name_en'] : '';
            $model->template = isset($data['template']) ? $data['template'] : '';
            $model->ticket_status = isset($data['ticket_status']) ? (int)$data['ticket_status'] : (int)1;
        }

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "จัดการข้อมูลบัตรคิว",
                    'content' => $this->renderAjax('_form_ticket', [
                        'model' => $model,
                    ]),
                    'footer' => '',

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'title' => "จัดการข้อมูลบัตรคิว",
                    'content' => '<span class="text-success">บันทึกสำเร็จ!</span>',
                    'footer' => Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]),
                    'status' => 200,
                    'url' => Url::to(['update', 'id' => (string)$model['_id']]),
                ];
            } else {
                return [
                    'title' => "จัดการข้อมูลบัตรคิว",
                    'content' => $this->renderAjax('_form_ticket', [
                        'model' => $model,
                    ]),
                    'footer' => '',
                    'status' => 422,
                    'validate' => ActiveForm::validate($model),
                ];
            }
        } else {
            return $this->render('_form_ticket', [
                'model' => $model,
            ]);
        }
    }

    public function actionCopyTicket($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            $oldModel = $this->findModelTicket($id);
            $newModel = new TbTicket();
            $newModel->isNewRecord = true;
            $newModel->attributes = $oldModel->attributes;
            if ($newModel->save()) {
                return $newModel;
            } else {
                throw new HttpException(422, Json::encode($newModel->errors));
            }
        } else {
            throw new MethodNotAllowedHttpException('method not allowed.');
        }
    }

    #จุดบริการ
    public function actionCounterService()
    {
        return $this->render('_content_counter_service');
    }

    public function actionDeleteCounterService($counter_service_type_id, $counter_service_id = null)
    {
        $request = Yii::$app->request;
        if ($counter_service_id != null) {
            TbCounterService::findOne($counter_service_id)->delete();
        }

        if (TbCounterService::find()->where(['counter_service_type_id' => $counter_service_type_id])->count() == 0) {
            TbCounterServiceType::findOne($counter_service_type_id)->delete();
        }

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->redirect(['counter-service']);
        }
    }

    public function actionCreateCounterService()
    {
        $request = Yii::$app->request;
        $model = new TbCounterServiceType();
        $modelCounterServices = [new TbCounterService()];

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "บันทึกจุดบริการ",
                    'content' => $this->renderAjax('_form_counter_service', [
                        'model' => $model,
                        'modelCounterServices' => (empty($modelCounterServices)) ? [new TbCounterService()] : $modelCounterServices,
                    ]),
                    'footer' => ''
                ];
            } elseif ($model->load($request->post())) {
                $oldIDs = ArrayHelper::map($modelCounterServices, '_id', '_id');
                $modelCounterServices = MultipleModel::createMultiple(TbCounterService::classname(), $modelCounterServices, '_id');
                MultipleModel::loadMultiple($modelCounterServices, $request->post());
                $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelCounterServices, '_id', '_id')));

                // validate all models
                $valid = $model->validate();
                $valid = Model::validateMultiple($modelCounterServices) && $valid;
                if ($valid) {
                    try {
                        if ($flag = $model->save(false)) {
                            if (!empty($deletedIDs) && is_array($deletedIDs)) {
                                foreach ($deletedIDs as $_id){
                                    if(!empty($_id)){
                                        TbCounterService::findOne((string)$_id)->delete();
                                    }
                                }
                            }
                            foreach ($modelCounterServices as $modelCounterService) {
                                $modelCounterService->counter_service_type_id = (string)$model['_id'];
                                if (!($flag = $modelCounterService->save(false))) {
                                    break;
                                }
                            }
                        }
                        if ($flag) {
                            return [
                                'title' => "บันทึกจุดบริการ",
                                'content' => '<span class="text-success">บันทึกสำเร็จ!</span>',
                                'footer' => Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]),
                                'status' => 200
                            ];
                        }
                    } catch (\Exception $e) {
                        throw new Exception($e->getMessage());
                    }
                } else {
                    return [
                        'title' => "บันทึกจุดบริการ",
                        'content' => $this->renderAjax('_form_counter_service', [
                            'model' => $model,
                            'modelCounterServices' => (empty($modelCounterServices)) ? [new TbCounterService()] : $modelCounterServices,
                        ]),
                        'footer' => '',
                        'validate' => ArrayHelper::merge(ActiveForm::validateMultiple($modelCounterServices), ActiveForm::validate($model)),
                        'status' => 422
                    ];
                }
            } else {
                return [
                    'title' => "บันทึกจุดบริการ",
                    'content' => $this->renderAjax('_form_counter_service', [
                        'model' => $model,
                        'modelCounterServices' => (empty($modelCounterServices)) ? [new TbCounterService()] : $modelCounterServices,
                    ]),
                    'footer' => '',
                    'validate' => ArrayHelper::merge(ActiveForm::validateMultiple($modelCounterServices), ActiveForm::validate($model)),
                    'status' => 422
                ];
            }
        } else {
            throw new MethodNotAllowedHttpException('method not allowed.');
        }
    }

    public function actionUpdateCounterService($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModelCounterServiceType($id);
        $modelCounterServices = TbCounterService::find()->where(['counter_service_type_id' => $id])->all();

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "บันทึกจุดบริการ",
                    'content' => $this->renderAjax('_form_counter_service', [
                        'model' => $model,
                        'modelCounterServices' => (empty($modelCounterServices)) ? [new TbCounterService()] : $modelCounterServices,
                    ]),
                    'footer' => ''
                ];
            } elseif ($model->load($request->post())) {
                $oldIDs = ArrayHelper::map($modelCounterServices, '_id', '_id');
                $modelCounterServices = MultipleModel::createMultiple(TbCounterService::classname(), $modelCounterServices, '_id');
                MultipleModel::loadMultiple($modelCounterServices, $request->post());
                $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelCounterServices, '_id', '_id')));

                // validate all models
                $valid = $model->validate();
                $valid = Model::validateMultiple($modelCounterServices) && $valid;
                if ($valid) {
                    try {
                        if ($flag = $model->save(false)) {
                            if (!empty($deletedIDs) && is_array($deletedIDs)) {
                                foreach ($deletedIDs as $_id){
                                    if(!empty($_id)){
                                        TbCounterService::findOne((string)$_id)->delete();
                                    }
                                }
                            }
                            foreach ($modelCounterServices as $modelCounterService) {
                                $modelCounterService->counter_service_type_id = (string)$model['_id'];
                                if (!($flag = $modelCounterService->save(false))) {
                                    break;
                                }
                            }
                        }
                        if ($flag) {
                            return [
                                'title' => "บันทึกจุดบริการ",
                                'content' => '<span class="text-success">บันทึกสำเร็จ!</span>',
                                'footer' => Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]),
                                'status' => 200
                            ];
                        }
                    } catch (\Exception $e) {
                        throw new Exception($e->getMessage());
                    }
                } else {
                    return [
                        'title' => "บันทึกจุดบริการ",
                        'content' => $this->renderAjax('_form_counter_service', [
                            'model' => $model,
                            'modelCounterServices' => (empty($modelCounterServices)) ? [new TbCounterService()] : $modelCounterServices,
                        ]),
                        'footer' => '',
                        'validate' => ArrayHelper::merge(ActiveForm::validateMultiple($modelCounterServices), ActiveForm::validate($model)),
                        'status' => 422
                    ];
                }
            } else {
                return [
                    'title' => "บันทึกจุดบริการ",
                    'content' => $this->renderAjax('_form_counter_service', [
                        'model' => $model,
                        'modelCounterServices' => (empty($modelCounterServices)) ? [new TbCounterService()] : $modelCounterServices,
                    ]),
                    'footer' => '',
                    'validate' => ArrayHelper::merge(ActiveForm::validateMultiple($modelCounterServices), ActiveForm::validate($model)),
                    'status' => 422
                ];
            }
        } else {
            throw new MethodNotAllowedHttpException('method not allowed.');
        }
    }

    #โปรแกรมเสียงเรียก
    public function actionSoundStation()
    {
        return $this->render('_content_sound_station');
    }

    public function actionDeleteSoundStation($id)
    {
        $request = Yii::$app->request;
        TbSoundStation::findOne($id)->delete();

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->redirect(['sound']);
        }
    }

    public function actionCreateSoundStation()
    {
        $request = Yii::$app->request;
        $model = new TbSoundStation();

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "โปรแกรมเสียง",
                    'content' => $this->renderAjax('_form_sound_station', [
                        'model' => $model,
                    ]),
                    'footer' => '',

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'title' => "โปรแกรมเสียง",
                    'content' => '<span class="text-success">บันทึกสำเร็จ!</span>',
                    'footer' => Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]),
                    'status' => 200,
                    'url' => Url::to(['update', 'id' => (string)$model['_id']]),
                ];
            } else {
                return [
                    'title' => "โปรแกรมเสียง",
                    'content' => $this->renderAjax('_form_sound_station', [
                        'model' => $model,
                    ]),
                    'footer' => '',
                    'status' => 422,
                    'validate' => ActiveForm::validate($model),
                ];
            }
        } else {
            throw new MethodNotAllowedHttpException('method not allowed.');
        }
    }

    public function actionUpdateSoundStation($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModelSoundStation($id);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "โปรแกรมเสียง",
                    'content' => $this->renderAjax('_form_sound_station', [
                        'model' => $model,
                    ]),
                    'footer' => '',

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'title' => "โปรแกรมเสียง",
                    'content' => '<span class="text-success">บันทึกสำเร็จ!</span>',
                    'footer' => Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]),
                    'status' => 200,
                    'url' => Url::to(['update', 'id' => (string)$model['_id']]),
                ];
            } else {
                return [
                    'title' => "โปรแกรมเสียง",
                    'content' => $this->renderAjax('_form_sound_station', [
                        'model' => $model,
                    ]),
                    'footer' => '',
                    'status' => 422,
                    'validate' => ActiveForm::validate($model),
                ];
            }
        } else {
            throw new MethodNotAllowedHttpException('method not allowed.');
        }
    }

    #เซอร์วิสโปรไฟล์
    public function actionServiceProfile()
    {
        return $this->render('_content_service_profile');
    }

    public function actionDeleteServiceProfile($id)
    {
        $request = Yii::$app->request;
        TbServiceProfile::findOne($id)->delete();

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->redirect(['sound']);
        }
    }

    public function actionCreateServiceProfile()
    {
        $request = Yii::$app->request;
        $model = new TbServiceProfile();

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "เซอร์วิสโปรไฟล์",
                    'content' => $this->renderAjax('_form_service_profile', [
                        'model' => $model,
                    ]),
                    'footer' => '',

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'title' => "เซอร์วิสโปรไฟล์",
                    'content' => '<span class="text-success">บันทึกสำเร็จ!</span>',
                    'footer' => Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]),
                    'status' => 200,
                ];
            } else {
                return [
                    'title' => "เซอร์วิสโปรไฟล์",
                    'content' => $this->renderAjax('_form_service_profile', [
                        'model' => $model,
                    ]),
                    'footer' => '',
                    'status' => 422,
                    'validate' => ActiveForm::validate($model),
                ];
            }
        } else {
            throw new MethodNotAllowedHttpException('method not allowed.');
        }
    }

    public function actionUpdateServiceProfile($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModelServiceProfile($id);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "เซอร์วิสโปรไฟล์",
                    'content' => $this->renderAjax('_form_service_profile', [
                        'model' => $model,
                    ]),
                    'footer' => '',

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'title' => "เซอร์วิสโปรไฟล์",
                    'content' => '<span class="text-success">บันทึกสำเร็จ!</span>',
                    'footer' => Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]),
                    'status' => 200,
                ];
            } else {
                return [
                    'title' => "เซอร์วิสโปรไฟล์",
                    'content' => $this->renderAjax('_form_service_profile', [
                        'model' => $model,
                    ]),
                    'footer' => '',
                    'status' => 422,
                    'validate' => ActiveForm::validate($model),
                ];
            }
        } else {
            throw new MethodNotAllowedHttpException('method not allowed.');
        }
    }

    #จอแสดงผล
    public function actionDisplay()
    {
        return $this->render('_content_display');
    }

    public function actionDeleteDisplay($id)
    {
        $request = Yii::$app->request;
        TbDisplay::findOne($id)->delete();

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->redirect(['sound']);
        }
    }

    public function actionCreateDisplay()
    {
        $request = Yii::$app->request;
        $model = new TbDisplay();
        $style = '';

        if ($model->load($request->post()) && !$request->isAjax) {
            $data = $request->post('TbDisplay', []);
            $style = strip_tags($data['display_css']);
            return $this->render('_form_display', [
                'model' => $model,
                'style' => $style,
            ]);
        } elseif ($request->isAjax && $model->load($request->post()) && $model->save()) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'url' => Url::to(['update-display', 'id' => (string)$model['_id']]),
                'model' => $model,
            ];
        } elseif ($request->isGet) {
            $model->display_css = $model->isNewRecord ? $model->defaultCss : $model->display_css;
            $style = strip_tags($model['display_css']);
            return $this->render('_form_display', [
                'model' => $model,
                'style' => $style,
            ]);
        } else {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'validate' => ActiveForm::validate($model),
            ];
        }
    }

    public function actionUpdateDisplay($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModelDisplay($id);
        $style = '';

        if ($model->load($request->post()) && !$request->isAjax) {
            $data = $request->post('TbDisplay', []);
            $style = strip_tags($data['display_css']);
            return $this->render('_form_display', [
                'model' => $model,
                'style' => $style,
            ]);
        } elseif ($request->isAjax && $model->load($request->post()) && $model->save()) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'url' => Url::to(['update-display', 'id' => (string)$model['_id']]),
                'model' => $model,
            ];
        } elseif ($request->isGet) {
            $style = strip_tags($model['display_css']);
            return $this->render('_form_display', [
                'model' => $model,
                'style' => $style,
            ]);
        } else {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'validate' => ActiveForm::validate($model),
            ];
        }
    }

    public function actionCopyDisplay($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            $oldModel = $this->findModelDisplay($id);
            $newModel = new TbDisplay();
            $newModel->isNewRecord = true;
            $newModel->attributes = $oldModel->attributes;
            if ($newModel->save()) {
                return $newModel;
            } else {
                throw new HttpException(422, Json::encode($newModel->errors));
            }
        } else {
            throw new MethodNotAllowedHttpException('method not allowed.');
        }
    }

    public function actionInsertDb()
    {
        $sounds = Yii::$app->db->createCommand('SELECT * FROM tb_sound')
            ->queryAll();
        /*foreach ($sounds as $sound){
            $model = new TbSound();
            $model->sound_name = $sound['sound_name'];
            $model->sound_path = $sound['sound_path_name'];
            $model->sound_label = $sound['sound_th'];
            $model->sound_type = $sound['sound_type'];
            $model->save();
        }*/
    }
}