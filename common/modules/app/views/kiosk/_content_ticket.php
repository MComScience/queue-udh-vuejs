<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 26/2/2562
 * Time: 11:23
 */

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

$this->registerCssFile("@web/css/checkbox-style.css", [
    'depends' => [\yii\bootstrap\BootstrapAsset::className()],
]);
?>
<div class="panel-body panel-kiosk" id="app">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL, 'id' => 'form-search', 'options' => ['autocomplete' => 'off']]); ?>
            <h2>
                <i class="fa fa-address-card-o"></i>
                <?= $model['service_group_name']; ?>
            </h2>
<!--            <span class="label label-warning">Notice <i class="fa fa-exclamation"></i> กรอกรหัส HN หรือรหัสบัตรประชาชนตามกลุ่มบริการที่ต้องการออกบัตรคิว / กด enter เพื่อค้นหา หรือคลิกที่ปุ่ม SEARCH</span>-->
            <div class="form-group">
                <label class="col-sm-2 control-label" for="tbque-search_by">ค้นหาโดย...</label>
                <div class="col-sm-10">
                    <div class="radio" style="display: inline-block;">
                        <label style="font-size: 1em">
                            <input v-on:change="onChangeCheckbox"
                                   type="radio"
                                   name="TbQue[search_by]"
                                   value="0"
                                   checked>
                            <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                            <?= Html::encode('HN') ?>
                        </label>
                    </div>
                    <div class="radio" style="display: inline-block;">
                        <label style="font-size: 1em">
                            <input v-on:change="onChangeCheckbox"
                                   type="radio"
                                   name="TbQue[search_by]"
                                   value="1">
                            <span class="cr"><i class="cr-icon fa fa-circle"></i></span>
                            <?= Html::encode('เลขบัตรประชาชน') ?>
                        </label>
                    </div>
                </div>
            </div>
            <p></p>
            <div class="form-group">
                <div class="col-md-12">
                    <?= Html::activeHiddenInput($model, '_id', []) ?>
                </div>
            </div>
            <div class="loader" v-if="loading">Loading...</div>
            <div v-if="!loading" v-for="(service, index) in services">
                <div class="form-group">
                    <div class="col-sm-12">
                        <p>
                            <span class="label label-success" style="font-size: 14px;">
                                {{ service.service_prefix }}: {{ service.service_name }}
                            </span>
                        </p>
                    </div>
                </div>
                <div class="input-group">
                    <div class="input-group-addon text-primary font-bold">{{ service.service_prefix }}</div>
                    <input class="form-control input-lg input-search"
                           ref="input"
                           :name="'TbQue[que_hn][' + service._id + ']'"
                           :id="service.service_prefix"
                           :placeholder="service.service_name"
                           :data-service_id="service._id"
                           :data-service_group_id="service.service_group_id"
                           :data-service_name="service.service_name"
                           :data-shortcut=" (index + 1) <= 12 ? 'F' + (index + 1) : ''"
                           v-on:keyup="onKeyupInputGroup" >
                    <div class="input-group-addon">
                        <span class="text-danger font-bold">{{ (index + 1) <= 12 ? 'F' + (index + 1) : '' }}</span>
<!--                        <button type="button"-->
<!--                                v-on:click="onClear"-->
<!--                                class="btn btn-danger">-->
<!--                            <i class="fa fa-close"></i>-->
<!--                            Clear-->
<!--                        </button>-->
                        <button v-bind:disabled="formIsValid"
                                v-on:click="onSubmit"
                                type="submit"
                                class="btn btn-success on-search-data hidden"
                                :id="'btn' + service._id">
                            <i class="fa fa-search"></i> SEARCH
                        </button>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>