<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 26/2/2562
 * Time: 11:20
 */

use yii\web\View;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use app\assets\SocketIOAsset;

SocketIOAsset::register($this);

$this->registerJsFile(
    YII_DEBUG ? '@web/vendor/vue/vue.js' : '@web/vendor/vue/vue.min.js',
    ['position' => View::POS_HEAD]
);
$this->registerJsFile(
    '@web/vendor/axios/axios.min.js',
    ['position' => View::POS_HEAD]
);
$this->registerCss(<<<CSS
.form-horizontal .radio,
.form-horizontal .checkbox,
.form-horizontal .radio-inline,
.form-horizontal .checkbox-inline {
    display: inline-block;
}
.input-group {
    margin-bottom: 10px;
}
input:focus {
    background-color: #ccc;
    text-transform: uppercase;
    color: #34495e;
    font-weight: bold;
}
.dataTables_length,.dt-buttons {
    float: left;
    display: inline-block;
}
div.dt-buttons {
    padding-left: 5px;
}
CSS
);
$this->title = $model['service_group_name'];
$this->params['breadcrumbs'][] = ['label' => 'ออกบัตรคิว', 'url' => ['/app/kiosk/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hpanel">
    <?php
    echo Tabs::widget([
        'items' => [
            [
                'label' => $model['service_group_name'],
                'content' => $this->render('_content_ticket', [
                    'model' => $model
                ]),
                'active' => true,
                'options' => ['id' => 'tab-ticket'],
            ],
            [
                'label' => 'รายการคิว ' . Html::tag('span', '0', ['class' => 'label label-success', 'id' => 'count-queue-data']),
                'content' => $this->render('_content_queue_list'),
                'options' => ['id' => 'tab-queue-list'],
            ]
        ],
        'encodeLabels' => false
    ]);
    ?>
</div>

<?php
echo $this->render('modal');

$this->registerJsFile(
    '@web/js/kiosk.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJsFile(
    '@web/js/keyboard-shortcuts.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>
