<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 26/2/2562
 * Time: 14:26
 */

use homer\widgets\Table;
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

?>

<div class="panel-body">
    <?php
    echo Table::widget([
        'tableOptions' => ['class' => 'table table-hover table-striped', 'id' => 'tb-queue-list'],
        'beforeHeader' => [
            [
                'columns' => [
                    ['content' => '#', 'options' => ['style' => 'text-align: center;width: 35px;']],
                    ['content' => 'หมายเลขคิว', 'options' => ['style' => 'text-align: center;']],
                    ['content' => 'HN', 'options' => ['style' => 'text-align: center;']],
                    ['content' => 'ชื่อ-นามสกุล', 'options' => ['style' => 'text-align: center;']],
                    ['content' => 'กลุ่มบริการ', 'options' => ['style' => 'text-align: center;']],
                    ['content' => 'ชื่อบริการ', 'options' => ['style' => 'text-align: center;']],
                    ['content' => 'เวลา', 'options' => ['style' => 'text-align: center;']],
                    ['content' => 'สถานะ', 'options' => ['style' => 'text-align: center;']],
                    ['content' => 'ดำเนินการ', 'options' => ['style' => 'text-align: center;']],
                ],
            ],
        ],
        'datatableOptions' => [
            "clientOptions" => [
                "dom" => "<'row'<'col-sm-6'l B><'col-sm-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-6'i><'col-sm-6'p>>",
                "ajax" => [
                    "url" => Url::base(true) . "/api/v1/data/data-queue",
                    "type" => "GET",
                    "complete" => new JsExpression('function(qXHR, textStatus ){
                        var api = $(\'#tb-queue-list\').DataTable();
                        api.buttons(0).processing( false );
                    }')
                ],
                "responsive" => true,
                "language" => [
                    "sLengthMenu" => "_MENU_",
                ],
                "autoWidth" => false,
                "deferRender" => true,
                "columns" => [
                    ["data" => "index", "className" => "text-center"],
                    ["data" => "queue_no_badge", "className" => "text-center"],
                    ["data" => "queue_hn", "className" => "text-center"],
                    ["data" => "pt_name"],
                    ["data" => "service_group_name"],
                    ["data" => "service_name"],
                    ["data" => "created_at", "className" => "text-center"],
                    ["data" => "que_status_name", "className" => "text-center"],
                    ["data" => "actions", "className" => "text-center", "orderable" => false],
                ],
                "drawCallback" => new JsExpression('function ( settings ) {
                    var api = this.api();
                    var count  = api.data().count();
                    $("#count-queue-data").html(count);
                    dtFunc.initConfirm("#tb-queue-list");
                }'),
                "buttons" => [
                    [
                        "text" => Icon::show('refresh').' Reload',
                        "action" =>  new JsExpression('function ( e, dt, node, config ) {
                            this.processing( true );
                            dt.ajax.reload();
                        }')
                    ],
                ]
            ],
            'clientEvents' => [
                'error.dt' => 'function ( e, settings, techNote, message ){
                    e.preventDefault();
                    swal({title: \'Error...!\',html: \'<small>\'+message+\'</small>\',type: \'error\',});
                }'
            ]
        ],
    ]);
    ?>
</div>