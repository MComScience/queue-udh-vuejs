<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 26/2/2562
 * Time: 14:57
 */

use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use kartik\icons\Icon;
use kartik\select2\Select2;
use kartik\widgets\DepDrop;
use homer\helpers\ArrayHelper;
use common\modules\app\models\TbService;
use common\modules\app\models\TbServiceGroup;
use yii\helpers\Url;

?>
<?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL, 'id' => 'form-queue']); ?>
    <div class="row">
        <div class="col-sm-6 col-sm-offset-2">
            <?= $form->field($model, 'service_group_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(TbServiceGroup::find()->asArray()->all(), '_id', 'service_group_name'),
                'language' => 'th',
                'options' => ['placeholder' => 'ประเภทเสียง'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'theme' => Select2::THEME_BOOTSTRAP,
            ])->label('กลุ่มบริการ'); ?>

            <?= $form->field($model, 'service_id')->widget(DepDrop::classname(), [
                'data' => ArrayHelper::map(TbService::find()->where(['service_group_id' => $model['service_group_id']])->asArray()->all(), '_id', 'service_name'),
                'type' => DepDrop::TYPE_SELECT2,
                'select2Options' => ['pluginOptions' => ['allowClear' => true], 'theme' => Select2::THEME_BOOTSTRAP],
                'pluginOptions' => [
                    'depends' => ['tbqueue-service_group_id'],
                    'placeholder' => 'Select...',
                    'url' => Url::to(['/app/kiosk/child-service-group'])
                ]
            ])->label('ประเภทบริการ'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8" style="text-align: right;">
            <?= Html::button(Icon::show('close') . 'ปิดหน้าต่าง', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']); ?>
            <?= Html::submitButton(Icon::show('save') . 'บันทึก', ['class' => 'btn btn-success']); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php
$this->registerJs(<<<JS
//Form Event
var table = $('#tb-queue-list').DataTable();
var \$form = $('#form-queue');
\$form.on('beforeSubmit', function() {
    var data = new FormData($(\$form)[0]);//\$form.serialize();
    var \$btn = $('button[type="submit"]').button('loading');//loading btn
    \$.ajax({
        url: \$form.attr('action'),
        type: 'POST',
        data: data,
        async: false,
        processData: false,
        contentType: false,
        success: function (data) {
            if(data.status == '200'){
                $('#ajaxCrudModal').modal('hide');//hide modal
                table.ajax.reload();//reload table
                swal({//alert completed!
                    type: 'success',
                    title: 'บันทึกสำเร็จ!',
                    showConfirmButton: false,
                    timer: 1500
                });
                setTimeout(function(){ \$btn.button('reset'); }, 1500);//clear button loading
            }else if(data.validate != null){
                $.each(data.validate, function(key, val) {
                    $(\$form).yiiActiveForm('updateAttribute', key, [val]);
                });
            }
            \$btn.button('reset');
        },
        error: function(jqXHR, errMsg) {
            swal('Oops...',errMsg,'error');
            \$btn.button('reset');
        }
    });
    return false; // prevent default submit
});
JS
);
?>