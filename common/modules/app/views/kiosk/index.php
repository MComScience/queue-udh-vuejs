<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 26/2/2562
 * Time: 10:38
 */
use yii\helpers\Html;

$this->title = 'ออกบัตรคิว';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <?php foreach ($serviceGroups as $serviceGroup): ?>
        <div class="col-md-3">
            <div class="hpanel">
                <div class="panel-body">
                    <div class="text-center">
                        <h2 class="m-b-xs"><?= $serviceGroup['service_group_name']; ?></h2>
                        <div class="m">
                            <i class="pe-7s-id fa-5x"></i>
                        </div>
                        <p class="small"></p>
                        <?= Html::a('Open',['/app/kiosk/issue-card', 'id' => (string)$serviceGroup['_id']],['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>


