<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 26/2/2562
 * Time: 11:30
 */

use homer\widgets\Modal;
use homer\assets\SweetAlert2Asset;

SweetAlert2Asset::register($this);

$this->registerCss('.modal-header {padding: 15px;}');

Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",
    'options' => ['class' => 'modal modal-danger', 'tabindex' => false,],
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false]
]);

Modal::end();
?>