<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 25/2/2562
 * Time: 21:57
 */

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\icons\Icon;
use yii\helpers\ArrayHelper;
use common\modules\app\models\TbCounterService;
use common\modules\app\models\TbCounterServiceType;

$this->registerCss(<<<CSS
.heading-controls {
    padding: 10px;
}
.modal-header {
    padding: 10px;
}
.modal-title {
    font-size: 20px;
}
.radio label, .checkbox label {
    padding-left: 0px;
}
CSS
);
$this->registerCssFile("@web/css/checkbox-style.css", [
    'depends' => [\yii\bootstrap\BootstrapAsset::className()],
]);
$options = [];
$services = TbCounterService::find()->where(['counter_service_status' => '1'])->all();
foreach ($services as $service) {
    $serviceType = TbCounterServiceType::findOne($service['counter_service_type_id']);
    $options[] = [
        'counter_service_id' => (string)$service['_id'],
        'counter_service_name' => '(' . $serviceType['counter_service_type_name'] . ') ' . $service['counter_service_name'],
    ];
}
?>

<?php $form = ActiveForm::begin([
    'id' => 'form-sound-station', 'type' => ActiveForm::TYPE_VERTICAL,
]); ?>

<div class="row">
    <div class="col-sm-4 col-sm-offset-2">
        <?= $form->field($model, 'sound_station_name')->textInput([])->label('ชื่อ'); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-8 col-sm-offset-2">
        <?= $form->field($model, 'counter_service_id')->checkBoxList(
            ArrayHelper::map($options, 'counter_service_id', 'counter_service_name'), [
            'inline' => false,
            'item' => function ($index, $label, $name, $checked, $value) {

                $return = '<div class="checkbox"><label style="font-size: 1em">';
                $return .= Html::checkbox($name, $checked, ['value' => $value]);
                $return .= '<span class="cr"><i class="cr-icon cr-icon glyphicon glyphicon-ok"></i></span>' . ucwords($label);
                $return .= '</label></div>';

                return $return;
            }
        ])->label('จุดบริการ'); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-4 col-sm-offset-2">
        <?= $form->field($model, 'sound_station_status')->RadioList(
            [0 => 'ปิดใช้งาน', 1 => 'เปิดใช้งาน'], [
            'inline' => true,
            'item' => function ($index, $label, $name, $checked, $value) {

                $return = '<div class="radio" style="display: inline-block"><label style="font-size: 1em">';
                $return .= Html::radio($name, $checked, ['value' => $value]);
                $return .= '<span class="cr"><i class="cr-icon fa fa-circle"></i></span>' . ucwords($label);
                $return .= '</label></div>';

                return $return;
            }
        ])->label('สถานะ'); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 text-right">
        <?= Html::button(Icon::show('close') . 'ปิดหน้าต่าง', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']); ?>
        <?= Html::submitButton(Icon::show('save') . 'บันทึก', ['class' => 'btn btn-success']); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>


<?php
$this->registerJs(<<<JS
var table = $('#tb-sound-station').DataTable();
var \$form = $('#form-sound-station');
\$form.on('beforeSubmit', function() {
    var data = new FormData($(\$form)[0]);//\$form.serialize();
    var \$btn = $('button[type="submit"]').button('loading');//loading btn
    \$.ajax({
        url: \$form.attr('action'),
        type: 'POST',
        data: data,
        async: false,
        processData: false,
        contentType: false,
        success: function (data) {
            if(data.status == '200'){
                $('#ajaxCrudModal').modal('hide');//hide modal
                table.ajax.reload();//reload table
                swal({//alert completed!
                    type: 'success',
                    title: 'บันทึกสำเร็จ!',
                    showConfirmButton: false,
                    timer: 1500
                });
                setTimeout(function(){ 
                    \$btn.button('reset');
                }, 1000);//clear button loading
            }else if(data.validate != null){
                $.each(data.validate, function(key, val) {
                    $(\$form).yiiActiveForm('updateAttribute', key, [val]);
                });
                \$btn.button('reset');
            }
        },
        error: function(jqXHR, errMsg) {
            swal('Oops...',errMsg,'error');
            \$btn.button('reset');
        }
    });
    return false; // prevent default submit
});

JS
);
?>