<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 24/2/2562
 * Time: 16:50
 */

use kartik\icons\Icon;
use yii\helpers\Html;
use yii\web\JsExpression;
use homer\widgets\Table;

$this->params['breadcrumbs'][] = ['label' => 'ตั้งค่า', 'url' => ['/app/settings/service-group']];
$this->params['breadcrumbs'][] = ['label' => 'ระบบคิว', 'url' => ['/app/settings/service-group']];
$this->params['breadcrumbs'][] = 'กลุ่มบริการ';
?>
<div class="hpanel">
    <?= $this->render('tabs'); ?>
    <div class="tab-content">
        <div id="tab-service-group" class="tab-pane active">
            <div class="panel-body">
                <?php
                echo Table::widget([
                    'tableOptions' => ['class' => 'table table-hover table-striped', 'id' => 'tb-service-group'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => '#', 'options' => ['style' => 'text-align: center;width: 35px;']],
                                ['content' => 'ชื่อกลุ่มบริการ', 'options' => ['style' => 'text-align: center;']],
                                ['content' => 'ชื่อบริการ', 'options' => ['style' => 'text-align: center;']],
                                ['content' => 'แบบพิมพ์บัตรคิว', 'options' => ['style' => 'text-align: center;']],
                                ['content' => 'จำนวนพิมพ์/ครั้ง', 'options' => ['style' => 'text-align: center;']],
                                ['content' => 'ตัวอักษร/ตัวเลข นำหน้าคิว', 'options' => ['style' => 'text-align: center;']],
                                ['content' => 'จำนวนหลักเลขคิว', 'options' => ['style' => 'text-align: center;']],
                                ['content' => 'เวลารอ(นาที)', 'options' => ['style' => 'text-align: center;']],
                                ['content' => 'เวลารอเผื่อ(นาที)', 'options' => ['style' => 'text-align: center;']],
                                ['content' => 'สถานะ', 'options' => ['style' => 'text-align: center;']],
                                ['content' => 'ดำเนินการ', 'options' => ['style' => 'text-align: center;']],
                            ],
                        ],
                    ],
                    'datatableOptions' => [
                        "clientOptions" => [
                            "dom" => "<'row'<'col-sm-6'l B><'col-sm-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-6'i><'col-sm-6'p>>",
                            "ajax" => [
                                "url" => "/api/v1/data/data-service-group",
                                "type" => "GET",
                                "complete" => new JsExpression('function(qXHR, textStatus ){
                                    var api = $(\'#tb-service-group\').DataTable();
                                    api.buttons(0).processing( false );
                                }')
                            ],
                            "lengthMenu" => [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
                            "responsive" => true,
                            "autoWidth" => false,
                            "columns" => [
                                ["data" => "index", "className" => "text-center"],
                                ["data" => "service_group_name"],
                                ["data" => "service_name"],
                                ["data" => "hos_name_th"],
                                ["data" => "print_copy_qty", "className" => "text-center"],
                                ["data" => "service_prefix", "className" => "text-center"],
                                ["data" => "service_numdigit", "className" => "text-center"],
                                ["data" => "avg_time", "className" => "text-center"],
                                ["data" => "avg_time_more", "className" => "text-center"],
                                ["data" => "service_status", "className" => "text-center"],
                                ["data" => "actions", "className" => "text-center nowrap", "orderable" => false],
                            ],
                            "drawCallback" => new JsExpression('function ( settings ) {
                                var api = this.api();
                                var rows = api.rows( {page:"current"} ).nodes();
                                var columns = api.columns().nodes();
                                var last=null;
                                api.column(1, {page:"current"} ).data().each( function ( group, i ) {
                                    var data = api.rows(i).data();
                                    if ( last !== group ) {
                                        $(rows).eq( i ).before(
                                            \'<tr class="warning"><td colspan="\'+columns.length+\'"><b>กลุ่มบริการ:</b> \'+group+\' <a href="/app/settings/update-service-group?id=\'+data[0].service_group_id+\'" class="btn btn-xs btn-success" role="modal-remote"><i class="fa fa-plus"></i> เพิ่มรายการ</a> </td></tr>\'
                                        );
                                        last = group;
                                    }
                                } );
                                dtFunc.initConfirm("#tb-service-group");
                            }'),
                            "columnDefs" => [
                                ["visible" => false, "targets" => 1],
                            ],
                            "language" => [
                                "sSearch" => Html::a(Icon::show('plus') . ' เพิ่มรายการ', ['/app/settings/create-service-group'], ['class' => 'btn btn-success btn-sm', 'role' => 'modal-remote']).' _INPUT_ ',
                                "sLengthMenu" => "_MENU_",
                            ],
                            "buttons" => [
                                [
                                    "text" => Icon::show('refresh').' Reload',
                                    "action" =>  new JsExpression('function ( e, dt, node, config ) {
                                        this.processing( true );
                                        dt.ajax.reload();
                                    }')
                                ],
                            ]
                        ],
                        'clientEvents' => [
                            'error.dt' => 'function ( e, settings, techNote, message ){
                                e.preventDefault();
                                swal({title: \'Error...!\',html: \'<small>\'+message+\'</small>\',type: \'error\',});
                            }'
                        ]
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>
