<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 26/2/2562
 * Time: 9:12
 */
use homer\widgets\Table;
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\web\JsExpression;

$this->params['breadcrumbs'][] = ['label' => 'ตั้งค่า', 'url' => ['/app/settings/counter-service']];
$this->params['breadcrumbs'][] = ['label' => 'ระบบคิว', 'url' => ['/app/settings/counter-service'],'data-toggle' => 'tab'];
$this->params['breadcrumbs'][] = 'จอแสดงผล';
?>
    <div class="hpanel">
        <?= $this->render('tabs'); ?>
        <div class="tab-content">
            <div id="tab-display" class="tab-pane active">
                <div class="panel-body">
                    <?php
                    echo Table::widget([
                        'tableOptions' => ['class' => 'table table-hover table-striped','id' => 'tb-display'],
                        'beforeHeader' => [
                            [
                                'columns' => [
                                    ['content' => '#', 'options' => ['style' => 'text-align: center;width: 35px;']],
                                    ['content' => 'ชื่อจอแสดงผล', 'options' => ['style' => 'text-align: center;']],
                                    ['content' => 'สถานะ', 'options' => ['style' => 'text-align: center;']],
                                    ['content' => 'ดำเนินการ', 'options' => ['style' => 'text-align: center;']],
                                ],
                            ],
                        ],
                        'datatableOptions' => [
                            "clientOptions" => [
                                "dom" => "<'row'<'col-sm-6'l B><'col-sm-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-6'i><'col-sm-6'p>>",
                                "ajax" => [
                                    "url" => "/api/v1/data/data-display",
                                    "type" => "GET",
                                    "complete" => new JsExpression('function(qXHR, textStatus ){
                                        var api = $(\'#tb-display\').DataTable();
                                        api.buttons(0).processing( false );
                                    }')
                                ],
                                "lengthMenu" => [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
                                "responsive" => true,
                                "language" => [
                                    "sSearch" => Html::a(Icon::show('plus') . ' เพิ่มรายการ', ['/app/settings/create-display'], ['class' => 'btn btn-success btn-sm']).' _INPUT_',
                                    "sLengthMenu" => "_MENU_",
                                ],
                                "autoWidth" => false,
                                "deferRender" => true,
                                "columns" => [
                                    ["data" => "index", "className" => "text-center"],
                                    ["data" => "display_name"],
                                    ["data" => "display_status","className" => "text-center"],
                                    ["data" => "actions", "className" => "text-center", "orderable" => false],
                                ],
                                "drawCallback" => new JsExpression('function ( settings ) {
                                    var api = this.api();
                                    var rows = api.rows( {page:"current"} ).nodes();
                                    var columns = api.columns().nodes();
                                    var last=null;
                                    dtFunc.initConfirm("#tb-display");
                                    $("a.activity-copy").on("click",function(event){
                                        event.preventDefault();
                                        QueSetting.dupplicate(this);
                                    });
                                }'),
                                "buttons" => [
                                    [
                                        "text" => Icon::show('refresh').' Reload',
                                        "action" =>  new JsExpression('function ( e, dt, node, config ) {
                                        this.processing( true );
                                        dt.ajax.reload();
                                    }')
                                    ],
                                ]
                            ],
                            'clientEvents' => [
                                'error.dt' => 'function ( e, settings, techNote, message ){
                                    e.preventDefault();
                                    swal({title: \'Error...!\',html: \'<small>\'+message+\'</small>\',type: \'error\',});
                                }'
                            ]
                        ],
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php
$this->registerJs(<<<JS
    QueSetting = {
        dupplicate: function(elm){
            swal({
                title: 'Duplicate?',
                text: "",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Confirm'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: "GET",
                        url: $(elm).attr("href"),
                        dataType: "json",
                        success: function(response){
                            dt_tbdisplay.ajax.reload();
                        },
                        error: function( jqXHR, textStatus, errorThrown){
                            swal({title: 'Error...!',text: errorThrown ,type: 'error'});
                        }
                    });
                }
            });
        }
    };
JS
);
?>