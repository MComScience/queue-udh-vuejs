<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 25/2/2562
 * Time: 14:15
 */
use homer\widgets\Table;
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\web\JsExpression;

$this->params['breadcrumbs'][] = ['label' => 'ตั้งค่า', 'url' => ['/app/settings/ticket']];
$this->params['breadcrumbs'][] = ['label' => 'ระบบคิว', 'url' => ['/app/settings/ticket'],'data-toggle' => 'tab'];
$this->params['breadcrumbs'][] = 'บัตรคิว';
?>

<div class="hpanel">
    <?= $this->render('tabs'); ?>
    <div class="tab-content">
        <div id="tab-ticket" class="tab-pane active">
            <div class="panel-body">
                <?php
                echo Table::widget([
                    'tableOptions' => ['class' => 'table table-hover table-striped','id' => 'tb-ticket'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => '#', 'options' => ['style' => 'text-align: center;width: 35px;']],
                                ['content' => 'ชื่อ รพ. ไทย', 'options' => ['style' => 'text-align: center;']],
                                ['content' => 'ชื่อ รพ. อังกฤษ','options' => ['style' => 'text-align: center;']],
                                ['content' => 'สถานะ', 'options' => ['style' => 'text-align: center;']],
                                ['content' => 'ดำเนินการ', 'options' => ['style' => 'text-align: center;']],
                            ],
                        ],
                    ],
                    'datatableOptions' => [
                        "clientOptions" => [
                            "dom" => "<'row'<'col-sm-6'l B><'col-sm-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-6'i><'col-sm-6'p>>",
                            "ajax" => [
                                "url" => "/api/v1/data/data-ticket",
                                "type" => "GET",
                                "complete" => new JsExpression('function(qXHR, textStatus ){
                                    var api = $(\'#tb-ticket\').DataTable();
                                    api.buttons(0).processing( false );
                                }')
                            ],
                            "lengthMenu" => [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
                            "responsive" => true,
                            "autoWidth" => false,
                            "deferRender" => true,
                            "columns" => [
                                ["data" => "index", "className" => "text-center"],
                                ["data" => "hos_name_th"],
                                ["data" => "hos_name_en"],
                                ["data" => "ticket_status", "className" => "text-center"],
                                ["data" => "actions", "className" => "text-center no-wrap", "orderable" => false],
                            ],
                            "drawCallback" => new JsExpression('function ( settings ) {
                                dtFunc.initConfirm("#tb-ticket");
                                $("a.activity-copy").on("click",function(event){
                                    event.preventDefault();
                                    QueSetting.copyTicket(this);
                                });
                            }'),
                            "language" => [
                                "sSearch" => Html::a(Icon::show('plus') . ' เพิ่มรายการ', ['/app/settings/create-ticket'], ['class' => 'btn btn-success btn-sm']).' _INPUT_',
                                "sLengthMenu" => "_MENU_",
                            ],
                            "buttons" => [
                                [
                                    "text" => Icon::show('refresh').' Reload',
                                    "action" =>  new JsExpression('function ( e, dt, node, config ) {
                                        this.processing( true );
                                        dt.ajax.reload();
                                    }')
                                ],
                            ]
                        ],
                        'clientEvents' => [
                            'error.dt' => 'function ( e, settings, techNote, message ){
                                e.preventDefault();
                                swal({title: \'Error...!\',html: \'<small>\'+message+\'</small>\',type: \'error\',});
                            }'
                        ]
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs(<<<JS
    QueSetting = {
        copyTicket: function(elm){
            swal({
                title: 'Copy บัตรคิว?',
                text: "",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Copy'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: "GET",
                        url: $(elm).attr("href"),
                        dataType: "json",
                        success: function(response){
                            dt_tbticket.ajax.reload();
                        },
                        error: function( jqXHR, textStatus, errorThrown){
                            swal({title: 'Error...!',text: errorThrown ,type: 'error'});
                        }
                    });
                }
            });
        }
    };
JS
);
?>