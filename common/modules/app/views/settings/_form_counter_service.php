<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 25/2/2562
 * Time: 15:57
 */

use yii\helpers\Html;
use kartik\form\ActiveForm;
use homer\widgets\dynamicform\DynamicFormWidget;
use kartik\icons\Icon;
use kartik\widgets\Select2;
use homer\helpers\ArrayHelper;
use common\modules\app\models\TbServiceGroup;
use common\modules\app\models\TbSound;

$this->registerCss(<<<CSS
#ajaxCrudModal .modal-dialog {
    width: 80%;
}
.heading-controls {
    padding: 10px;
}
.modal-header {
    padding: 10px;
}
.modal-title {
    font-size: 20px;
}
.radio label, .checkbox label {
    padding-left: 0px;
}
CSS
);
$this->registerCssFile("@web/css/checkbox-style.css", [
    'depends' => [\yii\bootstrap\BootstrapAsset::className()],
]);
$sounds = TbSound::find()->all();
$soundServiceOptions = [];
$soundNumberOptions = [];
foreach ($sounds as $sound) {
    if (strpos($sound['sound_name'], 'Service') !== false) {
        $soundServiceOptions[] = [
            '_id' => (string)$sound['_id'],
            'sound_name' => $sound['sound_name'].' ('.$sound['sound_label'].')'
        ];
    } else {
        $soundNumberOptions[] = [
            '_id' => (string)$sound['_id'],
            'sound_name' => $sound['sound_name'].' ('.$sound['sound_label'].')'
        ];
    }
}
?>
<?php $form = ActiveForm::begin([
    'id' => 'form-counter',
    'type' => ActiveForm::TYPE_VERTICAL,
]); ?>
    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'counter_service_type_name')->textInput([]); ?>
        </div>
    </div>
<?php DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_wrapper',
    'widgetBody' => '.container-items',
    'widgetItem' => '.item',
    'limit' => 20,
    'min' => 0,
    'insertButton' => '.add-item',
    'deleteButton' => '.remove-item',
    'model' => $modelCounterServices[0],
    'formId' => 'form-counter',
    'formFields' => [
        '_id',
        'counter_service_name',
        'counter_service_call_number',
        'counter_service_type_id',
        'service_group_id',
        'sound_number_id',
        'sound_service_id',
        'counter_service_status'
    ],
    'clientEvents' => [
        'afterInsert' => 'function(e, item) {
                jQuery(".dynamicform_wrapper .panel-title").each(function(index) {
                    jQuery(this).html("รายการที่ : " + (index + 1));
                });
            }',
        'afterDelete' => 'function(e, item) {
                jQuery(".dynamicform_wrapper .panel-title").each(function(index) {
                    jQuery(this).html("รายการที่ : " + (index + 1));
                });
            }'
    ],
]); ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <?= Icon::show('edit') . 'ช่องบริการย่อย'; ?>
            <?= Html::button(Icon::show('plus') . 'เพิ่มรายการ', ['class' => 'pull-right add-item btn btn-success btn-xs']); ?>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body container-items"><!-- widgetContainer -->
            <?php foreach ($modelCounterServices as $index => $modelCounterService): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                    <div class="heading-controls">
                        <?= Html::tag('span', 'รายการที่ : ' . ($index + 1), ['class' => 'panel-title']); ?>
                        <div style="float: right;">
                            <?= Html::button(Icon::show('minus'), ['class' => 'remove-item btn btn-danger btn-xs']); ?>
                            <?= Html::button(Icon::show('plus'), ['class' => 'add-item btn btn-success btn-xs']); ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?php
                        if (!$modelCounterService->isNewRecord) {
                            echo Html::activeHiddenInput($modelCounterService, "[{$index}]_id");
                        }
                        ?>
                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-1">
                                <?= $form->field($modelCounterService, "[{$index}]counter_service_name")->textInput([
                                    //'placeholder' => 'ชื่อช่องบริการ'
                                ]); ?>

                                <?= $form->field($modelCounterService, "[{$index}]service_group_id")->widget(Select2::classname(), [
                                    'data' => ArrayHelper::map(TbServiceGroup::find()->all(), '_id', 'service_group_name'),
                                    'options' => ['placeholder' => 'เลือกกลุ่มบริการ...'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                ]) ?>

                                <?= $form->field($modelCounterService, "[{$index}]sound_number_id")->widget(Select2::classname(), [
                                    'data' => ArrayHelper::map($soundNumberOptions, '_id', 'sound_name'),
                                    'options' => ['placeholder' => 'เลือกไฟล์เสียง...'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                ])->hint('<small class="text-danger">Prompt1 = เสียงผู้หญิง , Prompt2 = เสียงผู้ชาย</small>') ?>

                                <?= $form->field($modelCounterService, "[{$index}]counter_service_status")->RadioList(
                                    [0 => 'ปิดใช้งาน', 1 => 'เปิดใช้งาน'], [
                                    'inline' => true,
                                    'item' => function ($index, $label, $name, $checked, $value) {

                                        $return = '<div class="radio" style="display: inline-block;"><label style="font-size: 1em;padding-left: 0px;">';
                                        $return .= Html::radio($name, $checked, ['value' => $value]);
                                        $return .= '<span class="cr"><i class="cr-icon fa fa-circle"></i></span>' . ucwords($label);
                                        $return .= '</label></div>';

                                        return $return;
                                    }
                                ]); ?>
                            </div>

                            <div class="col-sm-4 col-sm-offset-1">
                                <?= $form->field($modelCounterService, "[{$index}]counter_service_call_number")->textInput([
                                    //'placeholder' => 'หมายเลข',
                                    'type' => 'number'
                                ]); ?>

                                <?= $form->field($modelCounterService, "[{$index}]sound_service_id")->widget(Select2::classname(), [
                                    'data' => ArrayHelper::map($soundServiceOptions, '_id', 'sound_name'),
                                    'options' => ['placeholder' => 'เลือกไฟล์เสียง...'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                    'theme' => Select2::THEME_BOOTSTRAP,
                                ])->hint('<small class="text-danger">Prompt1 = เสียงผู้หญิง , Prompt2 = เสียงผู้ชาย</small>') ?>
                            </div>
                        </div><!-- End FormGroup /-->
                    </div>
                </div>
            <?php endforeach; ?>
        </div><!-- End Body Panel /-->
    </div><!-- End Panel /-->
<?php DynamicFormWidget::end(); ?>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12" style="text-align: right;">
            <div class="form-group">
                <div class="col-sm-12">
                    <?= Html::button(Icon::show('close') . 'ปิดหน้าต่าง', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']); ?>
                    <?= Html::submitButton(Icon::show('save') . 'บันทึก', ['class' => 'btn btn-success']); ?>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>


<?php
$this->registerJs(<<<JS
//Form Event
var table = $('#tb-counter-service').DataTable();
var \$form = $('#form-counter');
\$form.on('beforeSubmit', function() {
    var data = new FormData($(\$form)[0]);//\$form.serialize();
    var \$btn = $('button[type="submit"]').button('loading');//loading btn
    \$.ajax({
        url: \$form.attr('action'),
        type: 'POST',
        data: data,
        async: false,
        processData: false,
        contentType: false,
        success: function (data) {
            if(data.status == '200'){
                $('#ajaxCrudModal').modal('hide');//hide modal
                table.ajax.reload();//reload table
                swal({//alert completed!
                    type: 'success',
                    title: 'บันทึกสำเร็จ!',
                    showConfirmButton: false,
                    timer: 1500
                });
                setTimeout(function(){ \$btn.button('reset'); }, 1500);//clear button loading
            }else if(data.validate != null){
                $.each(data.validate, function(key, val) {
                    $(\$form).yiiActiveForm('updateAttribute', key, [val]);
                });
            }
            \$btn.button('reset');
        },
        error: function(jqXHR, errMsg) {
            swal('Oops...',errMsg,'error');
            \$btn.button('reset');
        }
    });
    return false; // prevent default submit
});
JS
);
?>
