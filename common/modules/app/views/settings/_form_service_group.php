<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 25/2/2562
 * Time: 10:01
 */

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\icons\Icon;
use homer\widgets\dynamicform\DynamicFormWidget;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use yii\mongodb\Query;

$this->registerCss(<<<CSS
#ajaxCrudModal .modal-dialog {
    width: 80%;
}
.heading-controls {
    padding: 10px;
}
.modal-header {
    padding: 10px;
}
.modal-title {
    font-size: 20px;
}
.radio label, .checkbox label {
    padding-left: 0px;
}
CSS
);
$this->registerCssFile("@web/css/checkbox-style.css", [
    'depends' => [\yii\bootstrap\BootstrapAsset::className()],
]);

$tickets = (new Query())
    ->select(['_id', 'hos_name_th'])
    ->from('tb_ticket')
    ->all();
$mapTicket = [];

foreach ($tickets as $ticket){
    $mapTicket[] = [
        '_id' => (string) $ticket['_id'],
        'hos_name_th' => $ticket['hos_name_th'],
    ];
}
?>
<?php $form = ActiveForm::begin([
    'id' => 'form-service-group',
    'type' => ActiveForm::TYPE_VERTICAL,
]); ?>
<div class="row">
    <div class="col-sm-4">
        <?= $form->field($model, 'service_group_name')->textInput([
            'placeholder' => 'ชื่อกลุ่มบริการ'
        ])->label('ชื่อกลุ่มบริการ'); ?>
    </div>
</div>

<?php DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_wrapper',
    'widgetBody' => '.container-items',
    'widgetItem' => '.item',
    'limit' => 20,
    'min' => 0,
    'insertButton' => '.add-item',
    'deleteButton' => '.remove-item',
    'model' => $modelServices[0],
    'formId' => 'form-service-group',
    'formFields' => [
        'service_id',
        'service_name',
        'service_group_id',
        'print_template_id',
        'print_copy_qty',
        'service_prefix',
        'service_numdigit',
        'service_status',
    ],
    'clientEvents' => [
        'afterInsert' => 'function(e, item) {
            jQuery(".dynamicform_wrapper .panel-title").each(function(index) {
                jQuery(this).html("รายการที่ : " + (index + 1));
            });
        }',
        'afterDelete' => 'function(e, item) {
            jQuery(".dynamicform_wrapper .panel-title").each(function(index) {
                jQuery(this).html("รายการที่ : " + (index + 1));
            });
        }'
    ],
]); ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <?= Icon::show('edit') . 'กลุ่มบริการย่อย'; ?>
        <?= Html::button(Icon::show('plus') . 'เพิ่มรายการ', ['class' => 'pull-right add-item btn btn-success btn-xs']); ?>
        <div class="clearfix"></div>
    </div>
    <div class="panel-body container-items"><!-- widgetContainer -->
        <?php foreach ($modelServices as $index => $modelService): ?>
            <div class="item panel panel-default"><!-- widgetBody -->
                <div class="heading-controls">
                    <?= Html::tag('span', 'รายการที่ : ' . ($index + 1), ['class' => 'panel-title']); ?>
                    <div style="float: right;">
                        <?= Html::button(Icon::show('minus'), ['class' => 'remove-item btn btn-danger btn-xs']); ?>
                        <?= Html::button(Icon::show('plus'), ['class' => 'add-item btn btn-success btn-xs']); ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <?php
                    if (!$modelService->isNewRecord) {
                        echo Html::activeHiddenInput($modelService, "[{$index}]_id");
                        echo Html::activeHiddenInput($modelService, "[{$index}]service_group_id");
                    }
                    ?>

                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-1">
                            <?= $form->field($modelService, "[{$index}]service_name")->textInput([
                                //'placeholder' => 'ชื่อบริการ'
                            ]); ?>

                            <?= $form->field($modelService, "[{$index}]print_copy_qty")->textInput([
                                //'placeholder' => 'จำนวนพิมพ์/ครั้ง',
                                'type' => 'number',
                                'min' => 0
                            ]); ?>

                            <?= $form->field($modelService, "[{$index}]avg_time")->textInput([
                                //'placeholder' => '(นาที)',
                            ]); ?>

                            <?= $form->field($modelService, "[{$index}]service_numdigit")->textInput([
                                //'placeholder' => 'จำนวนหลักหมายเลขคิว',
                                'type' => 'number',
                                'min' => 0
                            ]); ?>

                            <?= $form->field($modelService, "[{$index}]service_status")->RadioList(
                                [0 => 'ปิดใช้งาน', 1 => 'เปิดใช้งาน'],[
                                'inline'=>true,
                                'item' => function($index, $label, $name, $checked, $value) {

                                    $return = '<div class="radio" style="display: inline-block;"><label style="font-size: 1em;padding-left: 0px;">';
                                    $return .= Html::radio( $name, $checked,['value' => $value]);
                                    $return .= '<span class="cr"><i class="cr-icon fa fa-circle"></i></span>' . ucwords($label);
                                    $return .= '</label></div>';

                                    return $return;
                                }
                            ]); ?>
                        </div>
                        <div class="col-sm-4 col-sm-offset-1">
                            <?= $form->field($modelService, "[{$index}]print_template_id")->widget(Select2::classname(), [
                                'data' => ArrayHelper::map($mapTicket,'_id','hos_name_th'),
                                'options' => ['placeholder' => 'เลือกแบบการพิมพ์บัตรคิว...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'theme' => Select2::THEME_BOOTSTRAP,
                            ]) ?>

                            <?= $form->field($modelService, "[{$index}]service_prefix")->textInput([
                                //'placeholder' => 'ตัวอักษร/ตัวเลข นำหน้าคิว'
                            ]); ?>

                            <?= $form->field($modelService, "[{$index}]avg_time_more")->textInput([
                                //'placeholder' => '(นาที)',
                            ]); ?>
                        </div>
                    </div><!-- end form-group -->

                </div>
            </div>
        <?php endforeach; ?>
    </div><!-- End panel body -->
</div><!-- end panel -->
<?php DynamicFormWidget::end(); ?>
<div class="row">
    <div class="col-sm-12 text-right">
        <?= Html::button(Icon::show('close') . 'ปิดหน้าต่าง', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']); ?>
        <?= Html::submitButton(Icon::show('save') . 'บันทึก', ['class' => 'btn btn-success']); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php
$this->registerJs(<<<JS
//Form Event
var table = $('#tb-service-group').DataTable();
var \$form = $('#form-service-group');
\$form.on('beforeSubmit', function() {
    var data = new FormData($(\$form)[0]);//\$form.serialize();
    var \$btn = $('button[type="submit"]').button('loading');//loading btn
    \$.ajax({
        url: \$form.attr('action'),
        type: 'POST',
        data: data,
        async: false,
        processData: false,
        contentType: false,
        success: function (data) {
            if(data.status == '200'){
                $('#ajaxCrudModal').modal('hide');//hide modal
                table.ajax.reload();//reload table
                swal({//alert completed!
                    type: 'success',
                    title: 'บันทึกสำเร็จ!',
                    showConfirmButton: false,
                    timer: 1500
                });
                setTimeout(function(){ \$btn.button('reset'); }, 1500);//clear button loading
            }else if(data.validate != null){
                $.each(data.validate, function(key, val) {
                    $(\$form).yiiActiveForm('updateAttribute', key, [val]);
                });
            }
            \$btn.button('reset');
        },
        error: function(jqXHR, errMsg) {
            swal('Oops...',errMsg,'error');
            \$btn.button('reset');
        }
    });
    return false; // prevent default submit
});
JS
);
?>
