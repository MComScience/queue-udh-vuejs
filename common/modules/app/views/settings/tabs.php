<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 25/2/2562
 * Time: 9:34
 */

use homer\widgets\Modal;
use yii\bootstrap\Tabs;
use yii\helpers\Url;
use homer\assets\SweetAlert2Asset;

SweetAlert2Asset::register($this);

$action = Yii::$app->controller->action->id;
$this->title = 'ตั้งค่า';

$this->registerCss(<<<CSS
.modal-header {
    padding: 15px;
}
.dataTables_length,.dt-buttons {
    float: left;
    display: inline-block;
}
div.dt-buttons {
    padding-left: 5px;
}
.btn-group {
    min-width: 100px;
    margin-left: 10%;
}
CSS
);

echo Tabs::widget([
    'items' => [
        [
            'label' => 'กลุ่มบริการ',
            'linkOptions' => ['id' => 'tab-service-group'],
            'url' => Url::to(['/app/settings/service-group']),
            'active' => $action == 'service-group' ? true : false,
        ],
        [
            'label' => 'บัตรคิว',
            'linkOptions' => ['id' => 'tab-ticket'],
            'url' => Url::to(['/app/settings/ticket']),
            'active' => $action == 'ticket' ? true : false,
        ],
        [
            'label' => 'จุดบริการ',
            'linkOptions' => ['id' => 'tab-counter-service'],
            'url' => Url::to(['/app/settings/counter-service']),
            'active' => $action == 'counter-service' ? true : false,
        ],
        [
            'label' => 'โปรแกรมเสียงเรียก',
            'linkOptions' => ['id' => 'tab-sound-station'],
            'url' => Url::to(['/app/settings/sound-station']),
            'active' => $action == 'sound-station' ? true : false,
        ],
        [
            'label' => 'เซอร์วิสโปรไฟล์',
            'linkOptions' => ['id' => 'tab-service-profile'],
            'url' => Url::to(['/app/settings/service-profile']),
            'active' => $action == 'service-profile' ? true : false,
        ],
        [
            'label' => 'จอแสดงผล',
            'linkOptions' => ['id' => 'tab-display'],
            'url' => Url::to(['/app/settings/display']),
            'active' => $action == 'display' ? true : false,
        ],
    ],
    'renderTabContent' => false,
    'encodeLabels' => false,
]);

Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",
    'options' => ['class' => 'modal modal-danger', 'tabindex' => false,],
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false]
]);

Modal::end();
?>