<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 26/2/2562
 * Time: 8:49
 */

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\icons\Icon;
use common\modules\app\models\TbCounterServiceType;
use homer\helpers\ArrayHelper;
use kartik\widgets\Select2;
use common\modules\app\models\TbService;
use common\modules\app\models\TbServiceGroup;

$this->registerCss(<<<CSS
.heading-controls {
    padding: 10px;
}
.modal-header {
    padding: 10px;
}
.modal-title {
    font-size: 20px;
}
.radio label, .checkbox label {
    padding-left: 0px;
}
CSS
);
$this->registerCssFile("@web/css/checkbox-style.css", [
    'depends' => [\yii\bootstrap\BootstrapAsset::className()],
]);
$serviceOptions = [];
$services = TbService::find()->where(['service_status' => '1'])->asArray()->all();
foreach ($services as $service){
    $serviceGroup = TbServiceGroup::findOne($service['service_group_id']);
    $serviceOptions[] = [
        '_id' => (string)$service['_id'],
        'service_name' => $serviceGroup['service_group_name'].' : '.$service['service_name']
    ];
}
?>

<?php $form = ActiveForm::begin([
    'id' => 'form-service-profile',
    'type' => ActiveForm::TYPE_VERTICAL,
]); ?>

<div class="row">
    <div class="col-sm-6 col-sm-offset-2">
        <?= $form->field($model, 'service_profile_name')->textInput([]); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6 col-sm-offset-2">
        <?= $form->field($model, 'counter_service_type_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(TbCounterServiceType::find()->asArray()->all(), '_id', 'counter_service_type_name'),
            'options' => ['placeholder' => 'เลือกเคาท์เตอร์...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
            'theme' => Select2::THEME_BOOTSTRAP,
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-10 col-sm-offset-2">
        <?= $form->field($model, 'service_id')->checkBoxList(
            ArrayHelper::map($serviceOptions,'_id', 'service_name'), [
            'inline' => false,
            'item' => function ($index, $label, $name, $checked, $value) {
                $return = '<div class="checkbox"><label style="font-size: 1em">';
                $return .= Html::checkbox($name, $checked, ['value' => $value]);
                $return .= '<span class="cr"><i class="cr-icon cr-icon glyphicon glyphicon-ok"></i></span>' . ucwords($label);
                $return .= '</label></div>';
                return $return;
            }
        ]); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-4 col-sm-offset-2">
        <?= $form->field($model, 'service_profile_status')->RadioList(
            [0 => 'ปิดใช้งาน', 1 => 'เปิดใช้งาน'], [
            'inline' => true,
            'item' => function ($index, $label, $name, $checked, $value) {

                $return = '<div class="radio" style="display: inline-block"> <label style="font-size: 1em">';
                $return .= Html::radio($name, $checked, ['value' => $value]);
                $return .= '<span class="cr"><i class="cr-icon fa fa-circle"></i></span>' . ucwords($label);
                $return .= '</label></div>';

                return $return;
            }
        ]); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 text-right">
        <?= Html::button(Icon::show('close') . 'ปิดหน้าต่าง', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']); ?>
        <?= Html::submitButton(Icon::show('save') . 'บันทึก', ['class' => 'btn btn-success']); ?>
    </div>
</div>

<?php ActiveForm::end(); ?>


<?php
$this->registerJs(<<<JS
var table = $('#tb-service-profile').DataTable();
var \$form = $('#form-service-profile');
\$form.on('beforeSubmit', function() {
    var data = new FormData($(\$form)[0]);//\$form.serialize();
    var \$btn = $('button[type="submit"]').button('loading');//loading btn
    \$.ajax({
        url: \$form.attr('action'),
        type: 'POST',
        data: data,
        async: false,
        processData: false,
        contentType: false,
        success: function (data) {
            if(data.status == '200'){
                $('#ajaxCrudModal').modal('hide');//hide modal
                table.ajax.reload();//reload table
                swal({//alert completed!
                    type: 'success',
                    title: 'บันทึกสำเร็จ!',
                    showConfirmButton: false,
                    timer: 1500
                });
                setTimeout(function(){ 
                    \$btn.button('reset');
                }, 1000);//clear button loading
            }else if(data.validate != null){
                $.each(data.validate, function(key, val) {
                    $(\$form).yiiActiveForm('updateAttribute', key, [val]);
                });
                \$btn.button('reset');
            }
        },
        error: function(jqXHR, errMsg) {
            swal('Oops...',errMsg,'error');
            \$btn.button('reset');
        }
    });
    return false; // prevent default submit
});
JS
);
?>