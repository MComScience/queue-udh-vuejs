<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 25/2/2562
 * Time: 16:51
 */
use homer\widgets\Table;
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\web\JsExpression;

$this->params['breadcrumbs'][] = ['label' => 'ตั้งค่า', 'url' => ['/app/settings/sound-station']];
$this->params['breadcrumbs'][] = ['label' => 'ระบบคิว', 'url' => ['/app/settings/sound-station'],'data-toggle' => 'tab'];
$this->params['breadcrumbs'][] = 'โปรแกรมเสียงเรียก';
?>

<div class="hpanel">
    <?= $this->render('tabs'); ?>
    <div class="tab-content">
        <div id="tab-sound-station" class="tab-pane active">
            <div class="panel-body">
                <?php
                echo Table::widget([
                    'tableOptions' => ['class' => 'table table-hover table-striped','id' => 'tb-sound-station'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => '#', 'options' => ['style' => 'text-align: center;width: 35px;']],
                                ['content' => 'ชื่อ', 'options' => ['style' => 'text-align: center;']],
                                ['content' => 'จุดบริการ','options' => ['style' => 'text-align: center;']],
                                ['content' => 'สถานะ', 'options' => ['style' => 'text-align: center;']],
                                ['content' => 'ดำเนินการ', 'options' => ['style' => 'text-align: center;']],
                            ],
                        ],
                    ],
                    'datatableOptions' => [
                        "clientOptions" => [
                            "dom" => "<'row'<'col-sm-6'l B><'col-sm-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-6'i><'col-sm-6'p>>",
                            "ajax" => [
                                "url" => "/api/v1/data/data-sound-station",
                                "type" => "GET",
                                "complete" => new JsExpression('function(qXHR, textStatus ){
                                    var api = $(\'#tb-sound-station\').DataTable();
                                    api.buttons(0).processing( false );
                                }')
                            ],
                            "lengthMenu" => [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
                            "responsive" => true,
                            "language" => [
                                "sSearch" => Html::a(Icon::show('plus') . ' เพิ่มรายการ', ['/app/settings/create-sound-station'], ['class' => 'btn btn-success btn-sm','role' => 'modal-remote']).' _INPUT_',
                                "sLengthMenu" => "_MENU_",
                            ],
                            "autoWidth" => false,
                            "deferRender" => true,
                            "columns" => [
                                ["data" => "index", "className" => "text-center"],
                                ["data" => "sound_station_name"],
                                ["data" => "counter_service_id"],
                                ["data" => "sound_station_status", "className" => "text-center"],
                                ["data" => "actions", "className" => "text-center no-wrap", "orderable" => false],
                            ],
                            "drawCallback" => new JsExpression('function ( settings ) {
                                dtFunc.initConfirm("#tb-sound-station");
                            }'),
                            "buttons" => [
                                [
                                    "text" => Icon::show('refresh').' Reload',
                                    "action" =>  new JsExpression('function ( e, dt, node, config ) {
                                        this.processing( true );
                                        dt.ajax.reload();
                                    }')
                                ],
                            ]
                        ],
                        'clientEvents' => [
                            'error.dt' => 'function ( e, settings, techNote, message ){
                                e.preventDefault();
                                swal({title: \'Error...!\',html: \'<small>\'+message+\'</small>\',type: \'error\',});
                            }'
                        ]
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>