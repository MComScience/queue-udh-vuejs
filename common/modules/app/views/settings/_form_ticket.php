<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 25/2/2562
 * Time: 14:48
 */
use yii\helpers\Html;
use kartik\form\ActiveForm;
use trntv\filekit\widget\Upload;
use kartik\widgets\Select2;
use yii\bootstrap\BootstrapAsset;
use kartik\icons\Icon;
use homer\assets\SweetAlert2Asset;

SweetAlert2Asset::register($this);

$this->title = 'บันทึกบัตรคิว';
$this->params['breadcrumbs'][] = ['label' => 'ตั้งค่า', 'url' => ['/app/setting/ticket']];
$this->params['breadcrumbs'][] = ['label' => 'ระบบคิว', 'url' => ['/app/setting/ticket']];
$this->params['breadcrumbs'][] = 'บัตรคิว';

$this->registerCssFile("@web/css/80mm.css", [
    'depends' => [BootstrapAsset::className()],
]);
$this->registerCssFile("@web/css/form-ticket.css", [
    'depends' => [BootstrapAsset::className()],
]);
$this->registerCssFile("@web/css/checkbox-style.css", [
    'depends' => [BootstrapAsset::className()],
]);
?>
<?= \mcomscience\sweetalert2\SweetAlert2::widget(['useSessionFlash' => true]) ?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?php $form = ActiveForm::begin([
                'id' => 'form-ticket', 'type' => ActiveForm::TYPE_HORIZONTAL,
                'formConfig' => ['showLabels' => false],
            ]);?>
            <div class="form-group">
                <?= Html::activeLabel($model, 'logo', ['label' => 'โลโก้บัตรคิว','class'=>'col-sm-2 control-label']) ?>
                <div class="col-sm-4">
                    <?= $form->field($model, 'logo')->widget(Upload::classname(), [
                        'url'=>['file-upload'],
                    ])->hint('<span class="text-warning">ภาพที่จะนำไปแสดงบนบัตรคิว</span>') ?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::activeLabel($model, 'hos_name_th', ['label' => 'ชื่อ รพ. (ไทย)','class'=>'col-sm-2 control-label']) ?>
                <div class="col-sm-4">
                    <?= $form->field($model, 'hos_name_th',['showLabels'=>false])->textInput([
                        'placeholder' => 'ชื่อ รพ. (ไทย)'
                    ]); ?>
                </div>

                <?= Html::activeLabel($model, 'hos_name_en', ['label' => 'ชื่อ รพ. (อังกฤษ)','class'=>'col-sm-2 control-label']) ?>
                <div class="col-sm-4">
                    <?= $form->field($model, 'hos_name_en',['showLabels'=>false])->textInput([
                        'placeholder' => 'ชื่อ รพ. (อังกฤษ)'
                    ]); ?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::activeLabel($model, 'ticket_status', ['class'=>'col-sm-2 control-label']) ?>
                <div class="col-sm-4">
                    <?= $form->field($model, 'ticket_status',['showLabels'=>false])->RadioList(
                        [0 => 'ปิดใช้งาน', 1 => 'เปิดใช้งาน'],[
                        'inline'=>true,
                        'item' => function($index, $label, $name, $checked, $value) {

                            $return = '<div class="radio"><label style="font-size: 1em;padding-left: 0px;">';
                            $return .= Html::radio( $name, $checked,['value' => $value]);
                            $return .= '<span class="cr"><i class="cr-icon fa fa-circle"></i></span>' . ucwords($label);
                            $return .= '</label></div>';

                            return $return;
                        }
                    ]); ?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::activeLabel($model, 'template', ['label' => 'บัตรคิว','class'=>'col-sm-2 control-label']) ?>
                <div class="col-sm-4">
                    <?= $form->field($model, 'template')->textarea([
                        'value' => $model->isNewRecord || empty($model['template']) ? $model->defaultTemplate : $model['template'],
                    ])->hint('<span class="text-danger">หมายเหตุ. ห้าม!!! เปลี่ยนข้อความที่มีเครื่องหมาย {} </span>') ?>
                </div>
                <?= Html::activeLabel($model, 'template', ['label' => 'ตัวอย่างบัตรคิว','class'=>'col-sm-2 control-label']) ?>
                <div class="col-sm-4">
                    <div id="editor-preview">
                        <?= $model->isNewRecord || empty($model['template']) ? $model->exampleTemplate : $model->ticketPreview; ?>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12" style="text-align: right;">
                    <?= Html::a(Icon::show('close').'ปิดหน้าต่าง',['/app/settings/ticket'],['class' => 'btn btn-default','data-dismiss' => 'modal']); ?>
                    <?= Html::submitButton(Icon::show('save').'บันทึก',['class' => 'btn btn-success']); ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
<?php
$this->registerJsFile(
    '@web/vendor/ckeditor/ckeditor.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerJsFile(
    '@web/vendor/moment/moment.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerJsFile(
    '@web/vendor/moment/locale/th.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJs(<<<JS
var d = new Date();
var y = d.getFullYear() + 543;
moment.locale('th');
var editor  = CKEDITOR.inline( 'tbticket-template',{
    contenteditable: true,
    language: 'th',
    extraPlugins: 'sourcedialog',
    uiColor: '#f1f3f6'
});

editor.on('change',function(){
    var data = editor.getData()
    .replace('{hos_name_th}', $('#tbticket-hos_name_th').val())
    .replace('{q_hn}','0008962222')
    .replace('{pt_name}','Hospital')
    .replace('{q_num}','A001')
    .replace('{pt_visit_type}','ผู้ป่วยนัดหมาย')
    .replace('{sec_name}','แผนกห้องยา')
    .replace('{time}',moment().format("D MMM ") + (y.toString()).substr(2))
    .replace('{user_print}','Admin Hospital');
    data.replace('{hos_name_th}', $('#tbticket-hos_name_th').val())
    $('#editor-preview').html(data);
    editor.updateElement();
});
JS
);
?>
<?php
if(Yii::$app->controller->action->id == 'update-ticket'){
    $this->registerJs(<<<JS
//var table = $('#tb-ticket').DataTable();
var \$form = $('#form-ticket');
\$form.on('beforeSubmit', function() {
    var data = new FormData($(\$form)[0]);//\$form.serialize();
    var \$btn = $('button[type="submit"]').button('loading');//loading btn
    \$.ajax({
        url: \$form.attr('action'),
        type: 'POST',
        data: data,
        async: false,
        processData: false,
        contentType: false,
        success: function (data) {
            if(data.status == '200'){
                $('#ajaxCrudModal').modal('hide');//hide modal
               // table.ajax.reload();//reload table
                swal({//alert completed!
                    type: 'success',
                    title: 'บันทึกสำเร็จ!',
                    showConfirmButton: false,
                    timer: 1500
                });
                setTimeout(function(){ 
                    \$btn.button('reset');
                }, 1000);//clear button loadingstatus
            }else if(data.validate != null){
                $.each(data.validate, function(key, val) {
                    $(\$form).yiiActiveForm('updateAttribute', key, [val]);
                });
                \$btn.button('reset');
            }
        },
        error: function(jqXHR, errMsg) {
            swal('Oops...',errMsg,'error');
            \$btn.button('reset');
        }
    });
    return false; // prevent default submit
});
JS
    );
}
?>