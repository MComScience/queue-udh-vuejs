<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 26/2/2562
 * Time: 8:40
 */
use homer\widgets\Table;
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\web\JsExpression;

$this->params['breadcrumbs'][] = ['label' => 'ตั้งค่า', 'url' => ['/app/settings/service-group']];
$this->params['breadcrumbs'][] = ['label' => 'ระบบคิว', 'url' => ['/app/settings/service-group'],'data-toggle' => 'tab'];
$this->params['breadcrumbs'][] = 'กลุ่มบริการ';

?>
<div class="hpanel">
    <?= $this->render('tabs'); ?>
    <div class="tab-content">
        <div id="tab-service-profile" class="tab-pane active">
            <div class="panel-body">
                <?php
                echo Table::widget([
                    'tableOptions' => ['class' => 'table table-hover table-striped','id' => 'tb-service-profile'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => '#', 'options' => ['style' => 'text-align: center;width: 35px;']],
                                ['content' => 'ชื่อโปรไฟล์', 'options' => ['style' => 'text-align: center;']],
                                ['content' => 'เคาท์เตอร์','options' => ['style' => 'text-align: center;']],
                                ['content' => 'เซอร์วิสบริการ', 'options' => ['style' => 'text-align: center;']],
                                ['content' => 'ดำเนินการ', 'options' => ['style' => 'text-align: center;']],
                            ],
                        ],
                    ],
                    'datatableOptions' => [
                        "clientOptions" => [
                            "dom" => "<'row'<'col-sm-6'l B><'col-sm-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-6'i><'col-sm-6'p>>",
                            "ajax" => [
                                "url" => "/api/v1/data/data-service-profile",
                                "type" => "GET",
                                "complete" => new JsExpression('function(qXHR, textStatus ){
                                    var api = $(\'#tb-service-profile\').DataTable();
                                    api.buttons(0).processing( false );
                                }')
                            ],
                            "lengthMenu" => [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
                            "responsive" => true,
                            "language" => [
                                "sSearch" => Html::a(Icon::show('plus') . ' เพิ่มรายการ', ['/app/settings/create-service-profile'], ['class' => 'btn btn-success btn-sm','role' => 'modal-remote']).' _INPUT_',
                                "sLengthMenu" => "_MENU_",
                            ],
                            "autoWidth" => false,
                            "deferRender" => true,
                            "columns" => [
                                ["data" => "index", "className" => "text-center"],
                                ["data" => "service_profile_name"],
                                ["data" => "counter_service_type_name"],
                                ["data" => "service_names"],
                                ["data" => "service_profile_status", "className" => "text-center"],
                                ["data" => "actions", "className" => "text-center", "orderable" => false],
                            ],
                            "drawCallback" => new JsExpression('function ( settings ) {
                                var api = this.api();
                                dtFunc.initConfirm("#tb-service-profile");
                            }'),
                            "buttons" => [
                                [
                                    "text" => Icon::show('refresh').' Reload',
                                    "action" =>  new JsExpression('function ( e, dt, node, config ) {
                                        this.processing( true );
                                        dt.ajax.reload();
                                    }')
                                ],
                            ]
                        ],
                        'clientEvents' => [
                            'error.dt' => 'function ( e, settings, techNote, message ){
                                e.preventDefault();
                                swal({title: \'Error...!\',html: \'<small>\'+message+\'</small>\',type: \'error\',});
                            }'
                        ]
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>