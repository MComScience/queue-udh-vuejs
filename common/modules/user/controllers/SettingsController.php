<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 23/2/2562
 * Time: 15:32
 */

namespace common\modules\user\controllers;

use common\components\AccessRule;
use common\modules\user\models\SettingsForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use common\modules\user\models\User;
use yii\web\Response;
use yii\widgets\ActiveForm;

class SettingsController extends Controller
{
    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'disconnect' => ['post'],
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className()
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['account', 'delete'],
                        'roles' => [
                            '@',
                        ],
                    ],
                ],
            ],
        ];
    }

    public function actionAccount()
    {
        /** @var SettingsForm $model */
        $model = \Yii::createObject(SettingsForm::className());
        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            \Yii::$app->response->data = ActiveForm::validate($model);
            \Yii::$app->response->send();
            \Yii::$app->end();
        }
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Your account details have been updated'));
            return $this->refresh();
        }

        return $this->render('account', [
            'model' => $model,
        ]);
    }
}