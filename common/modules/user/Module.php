<?php

namespace common\modules\user;

/**
 * user module definition class
 */
class Module extends \yii\base\Module
{
    /** Email is changed right after user enter's new email address. */
    const STRATEGY_INSECURE = 0;

    /** Email is changed after user clicks confirmation link sent to his new email address. */
    const STRATEGY_DEFAULT = 1;

    /** Email is changed after user clicks both confirmation links sent to his old and new email addresses. */
    const STRATEGY_SECURE = 2;

    /** @var int Email changing strategy. */
    public $emailChangeStrategy = self::STRATEGY_DEFAULT;
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'common\modules\user\controllers';

    public $cost = 10;

    public $debug = false;

    /** @var array Model map */
    public $modelMap = [];

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
