<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 23/2/2562
 * Time: 15:41
 */

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\icons\Icon;

Icon::map($this);

$this->title = 'การตั้งค่าบัญชี';
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= $this->title ?></h3>
    </div>
    <div class="panel-body">
        <?php $form = ActiveForm::begin([
            'id' => 'account-form',
            'type' => ActiveForm::TYPE_HORIZONTAL,
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'formConfig' => ['showLabels' => false]
        ]); ?>

        <div class="form-group">
            <?= Html::activeLabel($model, 'email', ['class' => 'col-sm-2 control-label']); ?>
            <div class="col-sm-4">
                <?= $form->field($model, 'email', ['addon' => ['prepend' => ['content' => Icon::show('envelope-open')]]])->textInput([
                    'placeholder' => $model->getAttributeLabel('email'),
                    'type' => 'email'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::activeLabel($model, 'username', ['class' => 'col-sm-2 control-label']); ?>
            <div class="col-sm-4">
                <?= $form->field($model, 'username', ['addon' => ['prepend' => ['content' => Icon::show('user')]]])->textInput([
                    'placeholder' => $model->getAttributeLabel('username')
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::activeLabel($model, 'new_password', ['class' => 'col-sm-2 control-label']); ?>
            <div class="col-sm-4">
                <?= $form->field($model, 'new_password', ['addon' => ['prepend' => ['content' => Icon::show('key')]]])->passwordInput([
                    'placeholder' => $model->getAttributeLabel('new_password'),
                ]) ?>
            </div>
        </div>

        <hr>

        <div class="form-group">
            <?= Html::activeLabel($model, 'current_password', ['class' => 'col-sm-2 control-label']); ?>
            <div class="col-sm-4">
                <?= $form->field($model, 'current_password', ['addon' => ['prepend' => ['content' => Icon::show('key')]]])->passwordInput([
                    'placeholder' => $model->getAttributeLabel('current_password'),
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2">
                <?= Html::submitButton(Icon::show('save') . 'บันทึก', ['class' => 'btn btn-success']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
