<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\icons\Icon;

/* @var $this yii\web\View */
/* @var $model common\modules\user\models\User */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?= $this->title ?></h3>
    </div>
    <div class="panel-body">
        <div class="user-form">

            <?php $form = ActiveForm::begin([
                'type' => ActiveForm::TYPE_HORIZONTAL,
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'formConfig' => ['showLabels' => false]
            ]); ?>

            <div class="form-group">
                <?= Html::activeLabel($model, 'username', ['class' => 'col-sm-2 control-label']); ?>
                <div class="col-sm-4">
                    <?= $form->field($model, 'username', ['addon' => ['prepend' => ['content' => Icon::show('user')]]])->textInput([
                        'placeholder' => $model->getAttributeLabel('username')
                    ]) ?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::activeLabel($model, 'email', ['class' => 'col-sm-2 control-label']); ?>
                <div class="col-sm-4">
                    <?= $form->field($model, 'email', ['addon' => ['prepend' => ['content' => Icon::show('envelope-open')]]])->textInput([
                        'placeholder' => $model->getAttributeLabel('email'),
                        'type' => 'email'
                    ]) ?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::activeLabel($model, 'password', ['class' => 'col-sm-2 control-label']); ?>
                <div class="col-sm-4">
                    <?= $form->field($model, 'password', ['addon' => ['prepend' => ['content' => Icon::show('key')]]])->passwordInput([
                        'placeholder' => $model->getAttributeLabel('password'),
                    ]) ?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::activeLabel($model, 'role', ['class' => 'col-sm-2 control-label']); ?>
                <div class="col-sm-4">
                    <?= $form->field($model, 'role', ['addon' => ['prepend' => ['content' => Icon::show('user')]]])->dropDownList([
                        10 => 'User',
                        20 => 'Admin'
                    ], [
                        'prompt' => 'เลือกบทบาท'
                    ]) ?>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <?= Html::a(Icon::show('times-circle') . 'กลับ', ['/user/admin/index'], ['class' => 'btn btn-danger']) ?>
                    <?= Html::submitButton(Icon::show('save') . 'บันทึก', ['class' => 'btn btn-success']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

