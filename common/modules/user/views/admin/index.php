<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\icons\Icon;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\user\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ผู้ใช้งาน';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'condensed' => true,
        'responsive'=>true,
        'hover'=>true,
        'pjax' => true,
        'tableOptions' => ['id' => 'tb-user', 'style' => 'width:100%'],
        'panel' => [
            'heading' => '<h3 class="panel-title">'.Icon::show('users').$this->title.'</h3>',
            'type' => 'default',
            'before' => Html::a('<i class="glyphicon glyphicon-plus"></i> เพิ่มผู้ใช้งาน', ['create'], ['class' => 'btn btn-success', 'data-pjax' => 0]),
            'after' => '',
            'footer' => ''
        ],
        'columns' => [
            [
                'class' => '\kartik\grid\SerialColumn'
            ],
            'username',
            'email',
            [
                'attribute' => 'status',
                'value' => function($model){
                    return $model->statusName;
                },
                'hAlign' => 'center'
            ],
            [
                'attribute' => 'role',
                'value' => function($model){
                    return $model->roleName;
                },
                'hAlign' => 'center'
            ],
            [
                'class' => '\kartik\grid\ActionColumn',
                'template' => '{update} {delete}',
                'noWrap' => true,
            ],
        ],
    ]); ?>
</div>