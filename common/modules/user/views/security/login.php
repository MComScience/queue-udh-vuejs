<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 23/2/2562
 * Time: 16:16
 */

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\icons\Icon;

Icon::map($this);

$this->title = 'Sign in';
?>
<div class="row">
    <div class="col-md-12">
        <div class="text-center m-b-md">
            <?= Html::img(Yii::getAlias('@web/images/udh-logo.png'),['class' => 'img-responsive center-block','width' => '200px']); ?>
            <h3 style="margin-top: 0px;margin-bottom: 0px;"><?= \Yii::$app->name ?></h3>
            <small></small>
        </div>
        <div class="hpanel">
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'login-form',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'validateOnBlur' => false,
                    'validateOnType' => false,
                    'validateOnChange' => false,
                ]) ?>


                <?= $form->field($model, 'username', [
                    'addon' => ['prepend' => ['content' => Icon::show('user')]]
                ])->textInput([
                    'autofocus' => 'autofocus',
                    'tabindex' => 1,
                    'placeholder' => 'ชื่อผู้ใช้งาน หรือ อีเมล'
                ])->label('ชื่อผู้ใช้งาน หรือ อีเมล');
                ?>

                <?= $form->field($model, 'password', [
                    'addon' => ['prepend' => ['content' => Icon::show('key')]]
                ])->passwordInput([
                    'tabindex' => 2,
                    'placeholder' => 'รหัสผ่าน'
                ])->label('รหัสผ่าน') ?>

                <?= $form->field($model, 'rememberMe')->checkbox(['tabindex' => '3']) ?>

                <?= Html::submitButton('เข้าสู่ระบบ',['class' => 'btn btn-primary btn-block', 'tabindex' => '4']) ?>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        <strong><?= Yii::$app->name; ?></strong><br/>
    </div>
</div>

