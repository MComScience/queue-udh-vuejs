<?php

namespace common\modules\user\models;

use Yii;
use yii\base\Model;
use common\modules\user\helpers\Password;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    protected $_user;

    public function getModule()
    {
        return \Yii::$app->getModule('user');
    }


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        $rules = [
            'usernameTrim' => ['username', 'trim'],
            'requiredFields' => [['username'], 'required'],
            'rememberMe' => ['rememberMe', 'boolean'],
        ];
        if (!$this->module->debug) {
            $rules = array_merge($rules, [
                'requiredFields' => [['username', 'password'], 'required'],
                'passwordValidate' => [
                    'password',
                    function ($attribute) {
                        if ($this->_user === null || !Password::validate($this->password, $this->_user->password_hash)) {
                            $this->addError($attribute, Yii::t('user', 'Invalid username or password'));
                        }
                    }
                ]
            ]);
        }
        return $rules;
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
      if ($this->_user === null || !Password::validate($this->password, $this->_user->password_hash))
        $this->addError($attribute, Yii::t('user', 'Invalid login or password'));
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate() && $this->_user) {
            $isLogged = Yii::$app->getUser()->login($this->_user, $this->rememberMe ? 3600*24*30 : 0);
            
            return $isLogged;
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $user = new User();
            $this->_user = $user->findUserByUsernameOrEmail(trim($this->username));
            return true;
        } else {
            return false;
        }
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'username'   => 'ชื่อผู้ใช้หรืออีเมล',
            'password'   => 'รหัสผ่าน',
            'rememberMe' => 'จำฉันในคราวต่อไป',
        ];
    }
}
