<?php

namespace common\modules\user\models;

use Yii;
use \yii\web\IdentityInterface;
use common\modules\user\helpers\Password;
use yii\base\NotSupportedException;
use yii\web\Application as WebApplication;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for collection "user".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $username
 * @property mixed $auth_key
 * @property mixed $password_hash
 * @property mixed $email
 * @property mixed $status
 * @property mixed $created_at
 * @property mixed $updated_at
 * @property mixed $role
 */
class User extends \yii\mongodb\ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    const ROLE_USER = 10;
    const ROLE_ADMIN = 20;

    /** @var string Plain password. Used for model validation. */
    public $password;

    /** @var string Default username regexp */
    public static $usernameRegexp = '/^[-a-zA-Z0-9_\.@]+$/';

    /**
     * {@inheritdoc}
     */
    public static function collectionName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function attributes()
    {
        return [
            '_id',
            'username',
            'auth_key',
            'password_hash',
            'email',
            'status',
            'created_at',
            'updated_at',
            'registration_ip',
            'role',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // username rules
            'usernameTrim'     => ['username', 'trim'],
            'usernameRequired' => ['username', 'required', 'on' => ['register', 'create', 'connect', 'update']],
            'usernameMatch'    => ['username', 'match', 'pattern' => static::$usernameRegexp],
            'usernameLength'   => ['username', 'string', 'min' => 3, 'max' => 255],
            'usernameUnique'   => [
                'username',
                'unique',
                'message' => \Yii::t('app', 'This username has already been taken')
            ],

            // email rules
            'emailTrim'     => ['email', 'trim'],
            'emailRequired' => ['email', 'required', 'on' => ['register', 'connect', 'create', 'update']],
            'emailPattern'  => ['email', 'email'],
            'emailLength'   => ['email', 'string', 'max' => 255],
            'emailUnique'   => [
                'email',
                'unique',
                'message' => \Yii::t('app', 'This email address has already been taken')
            ],
            // password rules
            'passwordRequired' => ['password', 'required','on' => ['register']],
            'passwordLength' => ['password', 'string', 'min' => 6, 'max' => 72, 'on' => ['register', 'create']],
            [['role'], 'required','on' => ['register', 'create', 'update']]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'username' => 'ชื่อผู้ใช้',
            'auth_key' => 'Auth Key',
            'password_hash' => 'รหัสผ่าน',
            'email' => 'อีเมล์',
            'status' => 'สถานะ',
            'created_at' => 'วันที่บันทึก',
            'updated_at' => 'วันที่แก้ไข',
            'registration_ip' => 'registration_ip',
            'role' => 'บทบาท',
            'password' => 'รหัสผ่าน'
        ];
    }

    /** @inheritdoc */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /** @inheritdoc */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        return ArrayHelper::merge($scenarios, [
            'register' => ['username', 'email', 'password','role'],
            'connect'  => ['username', 'email'],
            'create'   => ['username', 'email', 'password','role'],
            'update'   => ['username', 'email', 'password','role'],
            'settings' => ['username', 'email', 'password','role'],
        ]);
    }

    /** @inheritdoc */
    public function getId()
    {
        return $this->getAttribute('_id');
    }

    /** @inheritdoc */
    public function getAuthKey()
    {
        return $this->getAttribute('auth_key');
    }

    /** @inheritdoc */
    public function validateAuthKey($authKey)
    {
        return $this->getAttribute('auth_key') === $authKey;
    }

    /** @inheritdoc */
    public static function findIdentity($id)
    {
        if ($id === (array)$id && isset($id['$oid'])) $id = $id['$oid'];

        return static::findOne($id);
    }

    /** @inheritdoc */
    public function beforeSave($insert)
    {
        if ($insert) {
            $this->setAttribute('auth_key', \Yii::$app->security->generateRandomString());
            if (\Yii::$app instanceof WebApplication) {
                $this->setAttribute('registration_ip', \Yii::$app->request->userIP);
            }
        }
        if (!empty($this->password)) {
            $this->setAttribute('password_hash', Password::hash($this->password));
        }
        return parent::beforeSave($insert);
    }

    /**
     * Creates new user account. If Module::enableGeneratingPassword is set true, this method
     * will generate password.
     *
     * @return bool
     */
    public function create()
    {
        if ($this->getIsNewRecord() == false) {
            throw new \RuntimeException('Calling "' . __CLASS__ . '::' . __METHOD__ . '" on existing user');
        }
        try {
            $this->status = self::STATUS_ACTIVE;
            $this->password = $this->password == null ? Password::generate(8) : $this->password;
            if (!$this->save()) {
                return false;
            }
            return true;
        } catch (\Exception $e) {
            \Yii::warning($e->getMessage());
            throw $e;
        }
    }

    /** @inheritdoc */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('Method "' . __CLASS__ . '::' . __METHOD__ . '" is not implemented.');
    }

    public function findUserByUsername($username)
    {
        return $this->find()->where(['username' => $username])->one();
    }

    public function findUserByEmail($email)
    {
        return $this->find()->where(['email' => $email])->one();
    }


    public function findUserByUsernameOrEmail($usernameOrEmail)
    {
        if (filter_var($usernameOrEmail, FILTER_VALIDATE_EMAIL)) {
            return $this->findUserByEmail($usernameOrEmail);
        }
        return $this->findUserByUsername($usernameOrEmail);
    }

    public function getStatusName()
    {
        if ($this->status == self::STATUS_ACTIVE){
            return 'Active';
        }else{
            return 'DeActive';
        }
    }

    public function getRoleName()
    {
        if ($this->role == self::ROLE_ADMIN){
            return 'Admin';
        }else{
            return 'User';
        }
    }

}
