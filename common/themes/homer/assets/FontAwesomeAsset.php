<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 23/2/2562
 * Time: 16:38
 */
namespace homer\assets;

use yii\web\AssetBundle;

class FontAwesomeAsset extends AssetBundle
{
    public $sourcePath = '@bower/fontawesome';

    public $css = [
        'css/font-awesome.min.css',
    ];

    public $publishOptions = [
        'only' => [
            'fonts/*',
            'css/*',
        ],
    ];
}
