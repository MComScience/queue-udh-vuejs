<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 23/2/2562
 * Time: 16:32
 */
namespace homer\assets;

use yii\web\AssetBundle as BaseAsset;

class HomerAsset extends BaseAsset
{
    public $sourcePath = '@homer/assets/dist';

    public $css = [
        'vendor/metisMenu/dist/metisMenu.css',
        'vendor/animate.css/animate.css',
        'fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css',
        'fonts/pe-icon-7-stroke/css/helper.css',
        'styles/static_custom.css',
        'styles/style.css',
    ];

    public $js = [
        'vendor/slimScroll/jquery.slimscroll.min.js',
        'vendor/metisMenu/dist/metisMenu.min.js',
        'scripts/homer.js',
    ];

    public $depends = [
        'homer\assets\FontAwesomeAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}