<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 23/2/2562
 * Time: 16:45
 */

use app\widgets\Alert;
use yii\widgets\Breadcrumbs;
?>
<div class="normalheader transition animated fadeIn small-header">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right">
                <?=Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'tag' => 'ol',
                    'options' => ['class' => 'hbreadcrumb breadcrumb'],
                ])?>
            </div>
            <h2 class="font-light m-b-xs">
                <?= $this->title ?>
            </h2>
            <small></small>
        </div>
    </div>
</div>
<div class="content animate-panel">
    <?= Alert::widget(); ?>
    <?= $content; ?>

</div>
