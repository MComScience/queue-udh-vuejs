<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 23/2/2562
 * Time: 17:16
 */

use yii\helpers\Html;
use yii\helpers\Url;

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@homer/assets/dist');
?>
<?php $this->beginContent('@homer/views/layouts/base.php', ['class' => 'blank']); ?>
<!-- Simple splash screen-->
<div class="splash">
    <div class="color-line"></div>
    <div class="splash-title"><h1><?= Yii::$app->name; ?></h1>
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>
</div>

<div class="color-line"></div>

<div class="back-link">
    <?= Html::a('Back to Dashboard', ['/site/index'], ['class' => 'btn btn-primary']) ?>
</div>
<div class="login-container" style="padding-top: 0;" id="app">
    <?= $content; ?>
</div>
<?php $this->endContent(); ?>
