<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 23/2/2562
 * Time: 16:46
 */
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@homer/assets/dist');
?>
<?php $this->beginContent('@homer/views/layouts/base.php', ['class' => 'fixed-navbar fixed-sidebar']); ?>
    <!-- Simple splash screen-->
    <div class="splash">
        <div class="color-line"></div>
        <div class="splash-title"><h1><?= Yii::$app->name ?></h1>
            <div class="spinner">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
                <div class="rect5"></div>
            </div>
        </div>
    </div>
    <?= $this->render('header', ['directoryAsset' => $directoryAsset]); ?>
    <?= $this->render('menu', ['directoryAsset' => $directoryAsset]); ?>
    <div id="wrapper">
        <?= $this->render('content', ['content' => $content, 'directoryAsset' => $directoryAsset]) ?>
        <?= $this->render('footer', ['directoryAsset' => $directoryAsset]) ?>
    </div>
<?php $this->endContent(); ?>