<?php
/**
 * Created by PhpStorm.
 * User: Tanakorn
 * Date: 23/2/2562
 * Time: 16:50
 */

use yii\widgets\Menu;
use kartik\icons\Icon;

?>
<!-- Navigation -->
<aside id="menu">
    <div id="navigation">
        <div class="profile-picture">
            <a href="index.html">
                <img src="<?= Yii::getAlias('@web/images/default-avatar.png') ?>" class="img-circle m-b" alt="logo"
                     style="width: 76px;height: 76px;">
            </a>

            <?php if (!Yii::$app->user->isGuest): ?>
                <div class="stats-label text-color">
                    <span class="font-extra-bold font-uppercase"><?= Yii::$app->user->identity->username; ?></span>

                    <div class="dropdown">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                            <small class="text-muted">Founder of App <b class="caret"></b></small>
                        </a>
                        <ul class="dropdown-menu animated flipInX m-t-xs">
                            <li><a href="contacts.html">Contacts</a></li>
                            <li><a href="profile.html">Profile</a></li>
                            <li><a href="analytics.html">Analytics</a></li>
                            <li class="divider"></li>
                            <li><a href="login.html">Logout</a></li>
                        </ul>
                    </div>
                </div>
            <?php endif; ?>
        </div>

        <?php
        $template = '<a href="{url}"><span class="nav-label">{label}</span><span class="fa arrow"></span></a>';
        echo Menu::widget([
            'items' => [
                // Important: you need to specify url as 'controller/action',
                // not just as 'controller' even if default action is used.
                [
                    'label' => Icon::show('dashboard') . 'แดชบอร์ด',
                    'url' => ['/site/index'],
                ],
                [
                    'label' => Icon::show('credit-card') . 'ออกบัตรคิว',
                    'url' => ['/app/kiosk/index'],
                    'visible' => !Yii::$app->user->isGuest,
                ],
                // 'Products' menu item will be selected as long as the route is 'product/index'
                [
                    'label' => Icon::show('cogs') . 'ตั้งค่า', 'url' => '#',
                    'items' => [
                        [
                            'label' => Icon::show('cog') . 'ระบบคิว',
                            'url' => ['/app/settings/service-group']
                        ],
                        [
                            'label' => Icon::show('users') . 'ผู้ใช้งาน',
                            'url' => ['/user/admin/index']
                        ],
                    ],
                    'template' => $template
                ],
            ],
            'options' => [
                'id' => 'side-menu',
                'class' => 'nav'
            ],
            'labelTemplate' => '<span class="nav-label">{label}</span>',
            'linkTemplate' => '<a href="{url}"><span class="nav-label">{label}</span></a>',
            'submenuTemplate' => "\n<ul class=\"nav nav-second-level\">\n{items}\n</ul>\n",
            'encodeLabels' => false,
            'activateParents' => true,
        ]);
        ?>
        <!--<ul class="nav" id="side-menu">
            <li>
                <router-link to="/">
                    <span class="nav-label"><i class="fa fa-dashboard"></i> หน้าหลัก</span>
                </router-link>
            </li>
            <li>
                <router-link to="/user/admin/index">
                    <span class="nav-label"><i class="fa fa-users"></i> ผู้ใช้งาน</span>
                </router-link>
            </li>
        </ul>-->
    </div>
</aside>
