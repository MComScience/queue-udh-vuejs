<?php
namespace common\components;

use Yii;
use common\modules\user\models\User;
use yii\base\InvalidConfigException;
use Closure;

class AccessRule extends \yii\filters\AccessRule {

    /**
     * @inheritdoc
     */
    protected function matchRole($user)
    {
        $items = empty($this->roles) ? [] : $this->roles;

        if (empty($items)) {
            return true;
        }

        if ($user === false) {
            throw new InvalidConfigException('The user application component must be available to specify roles in AccessRule.');
        }
        $isGuest = $user->getIsGuest();
        foreach ($items as $item) {
            if ($item === '?' && $isGuest) {
                return true;
            } elseif ($item === '@' && !$isGuest) {
                return true;
            } elseif ($item == User::ROLE_USER && !$isGuest){
                return true;
            } elseif (!$isGuest && $item === (int)$user->identity->role){
                return true;
            }
        }

        return false;
    }
}