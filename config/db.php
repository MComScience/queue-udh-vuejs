<?php

return [
    /* 'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8', */
    'class' => '\yii\mongodb\Connection',
    'dsn' => 'mongodb://queue:123456@192.168.1.14:27017/queue-udh?authSource=admin',
    /* 'options' => [
        "username" => "Username",
        "password" => "Password"
    ] */
    'enableLogging' => true, // enable logging
    'enableProfiling' => true, // enable profiling

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
